

function rm(params){
    
    var mensajesWObj = require('consultas/recepcionMensajes/mensajes');
    var detalleWObj = require('consultas/recepcionMensajes/detalle');
    
    var mensajesW = new mensajesWObj();
    var detalleW = new detalleWObj();
    
    
    var window = Ti.UI.createWindow({
    title:'Recepción de Mensajes',
    backgroundColor:'white'
    });
    
    window.add(detalleW);
    window.add(mensajesW);
    
    return window;
}

module.exports=rm;
