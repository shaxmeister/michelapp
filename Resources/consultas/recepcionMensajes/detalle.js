function detail(){
    
   
    var detalleW = Ti.UI.createView({
        backgroundColor:'lightgray',
        right:0,
        width:'75%',
        layout:'vertical',
        borderWidth:1
    });
    
     var header = Ti.UI.createView({
        backgroundColor:'gray',
        height:25,
        top:0
    });
    
    var titleLbl = Ti.UI.createLabel({
        text:'Información del Mensaje',
        color:'white',
        font:{fontSize:'22'}
    });
    header.add(titleLbl);
    
    detalleW.add(header);
    
    
    var titleMsj = Ti.UI.createLabel({
        text:'titulo Mensaje',
        width:Ti.UI.FILL,
        height:20,
        color:'white',
        font:{fontSize:'22'},
        top:15
    });
    
    var textMsj = Ti.UI.createTextArea({
        text:'bla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla bla',
        color:'black',
        width:Ti.UI.FILL,
        height:450,
        top:15
    });
    
    detalleW.add(titleMsj);
    detalleW.add(textMsj);
    
    return detalleW;
}

module.exports=detail;