function msj(){
    
    var controller = require('consultas/recepcionMensajes/controller');
    
    var mensajesW = Ti.UI.createView({
        backgroundColor:'white',
        left:0,
        width:'25%',
        borderWidth:1,
        layout:'vertical'
    });
    
    var header = Ti.UI.createView({
        backgroundColor:'gray',
        height:25,
        top:0
    });
    
    var titleLbl = Ti.UI.createLabel({
        text:'Mensajes',
        color:'white',
        font:{fontSize:'22'}
    });
    header.add(titleLbl);
    
    
    mensajesW.add(header);
    
    
   var mensajesTable = controller.MsjsRow('jj');
    mensajesW.add(mensajesTable);
    
    return mensajesW;
}

module.exports=msj;