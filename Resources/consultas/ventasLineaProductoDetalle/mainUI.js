
function main(params){
    
    var controller = require('consultas/ventasLineaProductoDetalle/controllers');
    var detalleWindObj = require('consultas/ventasLineaProductoDetalle/detalleUI');
    
    var window = Ti.UI.createWindow({
    title:'Venta de Línea Producto Detalle-Línea',
    backgroundColor:'white',
    layout:'composite'
    });
    
    var mainView = Ti.UI.createView({
    height:Ti.UI.SIZE,
    width:900,
    top:20,
    left:20,
    borderWidth:2,
    backgroundColor:'transparent',
    layout:'vertical'
    });
    
    var rangoFechasLbl = controller.filaTexto(' Rango de Fechas de Factura');
    var deFechaLbl = controller.filaTexto(' De');
    var aFechaLbl = controller.filaTexto(' A');
    controller.filaPickers({
        origin:deFechaLbl,
        datePicker:true
    });
    controller.filaPickers({
        origin:aFechaLbl,
        datePicker:true
    });
    var tipoLbl = controller.filaTexto(' Tipo de Reporte');
    controller.filaPickers({
        origin:tipoLbl,
        datePicker:false
    });
    var clienteTf = controller.textFields({
        hint:' Cliente'
    });
    var desMercadoTf = controller.textFields({
        hint:' Desarrollador de Mercado'
    });
    var estadoTf = controller.textFields({
        hint:' Estado'
    });
    var paisTf = controller.textFields({
        hint:' País'
    });
    var lineaTf = controller.textFields({
        hint:' Línea'
    });
    var productoTf = controller.textFields({
        hint:' Grupo'
    });
    
    
    mainView.add(rangoFechasLbl);
    mainView.add(deFechaLbl);
    mainView.add(aFechaLbl);
    mainView.add(tipoLbl);
    mainView.add(clienteTf);
    mainView.add(desMercadoTf);
    mainView.add(estadoTf);
    mainView.add(paisTf);
    mainView.add(lineaTf);
    mainView.add(productoTf);
    
    var okButton = Ti.UI.createButton({
    bottom: 100,
    right:40,
    width:120,
    height:38,
    title:'Confirmar'
    });

    okButton.addEventListener('click',function(){
        
        var detWin = new detalleWindObj();
        params.openW(detWin);
    });


window.add(mainView);
window.add(okButton);
return window;
    
}


module.exports = main;