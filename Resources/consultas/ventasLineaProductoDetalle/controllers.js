

exports.filaTexto = function(title){
    
var mainLbl = Ti.UI.createLabel({
    text:title,
    font:{fontSize:16 ,fontWeight:'regular'},
    height:38,
    width:Ti.UI.FILL,
    top:0,
    color:'black',
    borderWidth:1,
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    left:0 
    }); 

return mainLbl;
    
};

exports.textFields = function(params){
    
    var Tf = Ti.UI.createTextField({
    top:0,
    left:0,
    borderWidth:1,
    paddingLeft :2,
    autocapitalization:false,
    autocorrect:false,
    width:Ti.UI.FILL,
    height:38,
    hintText:params.hint
});

return Tf;
};


exports.filaPickers = function(params){
    
    if(params.datePicker){
        var picker = Ti.UI.createPicker({
  type:Ti.UI.PICKER_TYPE_DATE,
  minDate:new Date(1984,0,1),
  maxDate:new Date(2034,11,31),
  value:new Date(2013,4,12),
  top:0
});

picker.addEventListener('change',function(e){
    var str=JSON.stringify(e.value);
    var arr = str.substr(1,10);
  //Ti.API.info("User selected date: " +arr);
  //Ti.API.info("User selected date: " +JSON.stringify(e.value));
  
  params.origin.setText("  "+arr);
});
    }else{
        var picker = Ti.UI.createPicker({
        top:0
        });

var data = [];
data[0]=Ti.UI.createPickerRow({title:'Resumen'});
data[1]=Ti.UI.createPickerRow({title:'Detalle'});
picker.add(data);
picker.selectionIndicator = true;

picker.addEventListener('change',function(e){
   // var arr = str.substr(1,10);
  Ti.API.info(e.row.title);
  //Ti.API.info("User selected date: " +JSON.stringify(e.value));
  
  origin.setText("  "+e.row.title);
});

    }
    
var  popover = Ti.UI.iPad.createPopover({ 
        width:300, 
        height:250,
        title:"Estatus",
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    popover.addEventListener('click',function(e){
        popover.hide();
    });
    popover.add(picker);
     params.origin.addEventListener('click',function(){
    popover.show({
        view:params.origin,
        animated:true
    }); 
    
    });

};