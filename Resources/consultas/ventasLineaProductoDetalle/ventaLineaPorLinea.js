
function main(params){
    
    var controller = require('consultas/ventasLineaProductoDetalle/controllers');
    
    var window = Ti.UI.createWindow({
    title:'Venta de Línea Producto Detalle-Línea',
    backgroundColor:'white',
    layout:'vertical'
    });
    
    var mainView = Ti.UI.createView({
    height:Ti.UI.SIZE,
    width:900,
    top:20,
    left:60,
    borderWidth:2,
    backgroundColor:'transparent',
    layout:'vertical'
    });
    
    var rangoFechasLbl = controller.filaTexto(' Rango de Fechas de Factura');
    var deFechaLbl = controller.filaTexto(' De');
    var aFechaLbl = controller.filaTexto(' A');
    controller.filaPickers({
        origin:deFechaLbl,
        datePicker:true
    });
    controller.filaPickers({
        origin:aFechaLbl,
        datePicker:true
    });
    var tipoLbl = controller.filaTexto(' Tipo de Reporte');
    controller.filaPickers({
        origin:tipoLbl,
        datePicker:false
    });
    var estadoTf = controller.textFields({
        hint:' Estado'
    });
    var paisTf = controller.textFields({
        hint:' País'
    });
    var lineaTf = controller.textFields({
        hint:' Línea'
    });
    var productoTf = controller.textFields({
        hint:' Grupo'
    });
    var artTf = controller.textFields({
        hint:' Artículo'
    });
    
    
    mainView.add(rangoFechasLbl);
    mainView.add(deFechaLbl);
    mainView.add(aFechaLbl);
    mainView.add(estadoTf);
    mainView.add(paisTf);
    mainView.add(lineaTf);
    mainView.add(productoTf);
    mainView.add(artTf);
    
    var okButton = Ti.UI.createButton({
    top: 0,
    right:60,
    width:120,
    height:38,
    title:'Confirmar'
    });
    
    var resultadosTitleLbl = Ti.UI.createLabel({
    text:'Ventas',
    font:{fontSize:22 ,fontWeight:'bold'},
    height:22,
    width:Ti.UI.SIZE,
    top:0,
    color:'navy',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    left:50 
    });

    // okButton.addEventListener('click',function(){
//         
    // });


window.add(mainView);
window.add(okButton);
window.add(resultadosTitleLbl);
return window;
    
}


module.exports = main;