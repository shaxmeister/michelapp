

function main(){
    
    var controller = require('consultas/ventasPorAgente/controller');
    
    var window = Ti.UI.createWindow({
    title:'Ventas por Agente',
    backgroundColor:'white',
    layout:'composite'
    });
    
    var filterBlock = Ti.UI.createView({
    height:Ti.UI.SIZE,
    width:450,
    top:30,
    left:30,
    borderWidth:2,
    borderRadius:9,
    backgroundColor:'gray',
    layout:'vertical'
    });
    var filtersHolder = Ti.UI.createView({
        top:10,
    height:Ti.UI.SIZE,
    width:400,
    borderWidth:2,
    borderRadius:9,
    backgroundColor:'lightgray',
    layout:'vertical'
    });
    var titleLbl = Ti.UI.createLabel({
    text:' Rango de Fechas de Factura',
    font:{fontSize:13 ,fontWeight:'regular'},
    height:40,
    width:400,
    top:0,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    left:0 
    });
    var deLbl = Ti.UI.createLabel({
    text:' De',
    font:{fontSize:13 ,fontWeight:'regular'},
    height:40,
    width:400,
    borderWidth:1,
    top:0,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    left:0 
    });
    
    controller.pickerFecha(deLbl);
    
    var aLbl = Ti.UI.createLabel({
    text:' A',
    font:{fontSize:13 ,fontWeight:'regular'},
    height:40,
    width:400,
    top:0,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    left:0 
    });
    controller.pickerFecha(aLbl);
    var confirmarButton = Ti.UI.createButton({
    top:20,
    right:0,
    width:120,
    height:38,
    title:'Confirmar'
    });
    
    var resultadosTitleLbl = Ti.UI.createLabel({
    text:'Ventas',
    font:{fontSize:20 ,fontWeight:'bold'},
    height:20,
    width:Ti.UI.SIZE,
    top:240,
    color:'#005890',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    left:50 
    });
    
    
    filtersHolder.add(titleLbl);
    filtersHolder.add(deLbl);
    filtersHolder.add(aLbl);
    filterBlock.add(filtersHolder);
    filterBlock.add(confirmarButton);
    
    window.add(filterBlock);
    window.add(resultadosTitleLbl);
    
    return window;
}

module.exports=main;
