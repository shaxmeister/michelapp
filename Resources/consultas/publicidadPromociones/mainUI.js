
function main(){
    
    
    var window = Ti.UI.createWindow({
        title:'Consulta de Publicidad/Promociones',
        backgroundColor: 'white'
    });
    
///////////////////////////////////////////////
//////      Left Side             ///////////
var leftView = Ti.UI.createView({
    left:0,
    width:'33%',
    height:Ti.UI.FILL,
    borderWidth:1,
    backgroundColor:'lightgray'
});

var table = Ti.UI.createTableView();
leftView.add(table);

///////////////////////////////////////////////
//////      Right Side             ///////////
    var rightView = Ti.UI.createView({
    right:0,
    width:'67%',
    height:Ti.UI.FILL,
    borderWidth:1,
    backgroundColor:'white',
    layout:'vertical'
    });

    var infoBlock = Ti.UI.createView({
        borderWidth:2,
        borderRadius:9,
        layout:'vertical',
        top:10,
        left:5,
        width:300,
        height:Ti.UI.SIZE
    });
    
  var  fvLbl = Ti.UI.createLabel({
    text:'Fecha de Vigencia',
    font:{fontSize:16 ,fontWeight:'regular'},
    height:33,
    width:300,
    top:0,
    left:0 
    });
  var  laLbl = Ti.UI.createLabel({
    text:'Línea de Artículo',
    font:{fontSize:16 ,fontWeight:'regular'},
    height:33,
    borderWidth:1,
    width:300,
    top:0,
    left:0 
    });
  var  ppLbl = Ti.UI.createLabel({
    text:'Publicidad/Promoción',
    font:{fontSize:16 ,fontWeight:'regular'},
    height:33,
    width:300,
    top:0,
    left:0 
    });
    
    infoBlock.add(fvLbl);
    infoBlock.add(laLbl);
    infoBlock.add(ppLbl);
    
   var ppImg = Ti.UI.createImageView({
    top:40,
    width:400,
    height:400,
    image:'',
    backgroundColor:'green'
    });
    
   var descripcionLbl = Ti.UI.createLabel({
    text:'Desscripciionn ',
    font:{fontSize:13 ,fontWeight:'regular'},
    height:80,
    width:Ti.UI.SIZE,
    top:5,
    left:0 
    });
    
  
    
    rightView.add(infoBlock);
    rightView.add(ppImg);
    rightView.add(descripcionLbl);
    
    window.add(leftView);
    window.add(rightView);
    return window;
}

module.exports = main;
