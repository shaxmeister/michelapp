
exports.previewArticulo = function(params){
    
    var mainView = Ti.UI.createView({
    height:Ti.UI.SIZE,
    width:130,
    top:20,
    left:20,
    backgroundColor:'white',
    layout:'vertical'
});

    var mainImg = Ti.UI.createImageView({
    top:10,
    width:100,
    height:100,
    touchEnabled:false,
    //image:'',
    backgroundColor:'green'
    });
    var descripcionLbl = Ti.UI.createLabel({
    text:'Descripción del Artículo',
    font:{fontSize:13 ,fontWeight:'regular'},
    height:Ti.UI.SIZE,
    touchEnabled:false,
    width:Ti.UI.FILL,
    top:10,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
    });
    var precioLbl = Ti.UI.createLabel({
    text:'$$$$$$$',
    font:{fontSize:13 ,fontWeight:'regular'},
    height:20,
    width:Ti.UI.FILL,
    touchEnabled:false,
    top:6,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
    });
    var detalleLbl = Ti.UI.createLabel({
    text:'Archivo',
    font:{fontSize:13 ,fontWeight:'regular'},
    height:20,
    width:Ti.UI.FILL,
    top:6,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    });
mainView.add(mainImg);
mainView.add(descripcionLbl);
mainView.add(precioLbl);
mainView.add(detalleLbl);

var  popover = Ti.UI.iPad.createPopover({ 
        width:300, 
        height:250,
        layout:'vertical',
        title:"Descripción",
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    
    var desc = descripcionRow();
    var numArt = descripcionRow();
    var unidadMed = descripcionRow();
    popover.add(desc);
    popover.add(numArt);
    popover.add(unidadMed);
    
    popover.addEventListener('click',function(e){
        popover.hide();
    });
     mainView.addEventListener('click',function(){
    popover.show({
        view:mainView,
        animated:true
    }); 
    
    });


return mainView;    
};

function descripcionRow(params){
    var row = Ti.UI.createView({
    height:30,
    width:300,
    top:0,
    left:0,
    backgroundColor:'white',
    layout:'horizontal'
});

var titleLbl = Ti.UI.createLabel({
    text:'titulo',
    font:{fontSize:16 ,fontWeight:'regular'},
    height:20,
    width:150,
    top:0,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    left:0 
    });
    var valueLbl = Ti.UI.createLabel({
    text:'valor',
    font:{fontSize:16 ,fontWeight:'regular'},
    height:20,
    width:150,
    top:0,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    right:0 
    });
    
    row.add(titleLbl);
    row.add(valueLbl);
    return row;
}
