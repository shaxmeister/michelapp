

function main(){
    
    var controller = require('consultas/deArticulos/controller');
    
    var window = Ti.UI.createWindow({
    title:'Consulta de Artículos',
    backgroundColor:'white',
    layout:'composite'
    });

///////////////////////////////////////////
/////       Left SIDE       ///////////

var leftView = Ti.UI.createView({
    height:Ti.UI.FILL,
    width:'33%',
    top:0,
    borderWidth:2,
    left:0,
    layout:'vertical'
});

var lineatf = Ti.UI.createTextField({
    top:0,
    left:0,
    paddingLeft:2,
    autocapitalization:false,
    autocorrect:false,
    width:Ti.UI.FILL,
    height:38,
    hintText:' Línea'
});

var preciosLbl = Ti.UI.createLabel({
    text:'Rango de Precios',
    font:{fontSize:20 ,fontWeight:'bold'},
    height:20,
    width:Ti.UI.FILL,
    top:0,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    left:0 ,
    borderWidth:1
    });
    var detf = Ti.UI.createTextField({
    top:0,
    left:0,
    paddingLeft:2,
    autocapitalization:false,
    autocorrect:false,
    width:Ti.UI.FILL,
    height:38,
    hintText:' De'
});
var atf = Ti.UI.createTextField({
    top:0,
    left:0,
    paddingLeft:2,
    borderWidth:1,
    autocapitalization:false,
    autocorrect:false,
    width:Ti.UI.FILL,
    height:38,
    hintText:' A'
});

   leftView.add(lineatf);
   leftView.add(preciosLbl);
   leftView.add(detf);
   leftView.add(atf);
   
///////////////////////////////////////////
/////       right    SIDE       ///////////

var rightView = Ti.UI.createView({
    height:Ti.UI.FILL,
    width:'67%',
    top:0,
    right:0,
    layout:'vertical'
}); 
var rightHeaderView = Ti.UI.createView({
    height:25,
    width:Ti.UI.FILL,
    top:0,
    backgroundColor:'lightgray',
    left:0,
    layout:'vertical'
});
var rightTitleLbl = Ti.UI.createLabel({
    text:'Detalle de Artículos',
    font:{fontSize:22 ,fontWeight:'bold'},
    height:21,
    width:Ti.UI.SIZE,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    });
    
    var scrollView = Ti.UI.createScrollView({
      contentWidth: 300,
      contentHeight: 'auto',
      backgroundColor:'gray',
      top:50,
      layout:'horizontal',
      showVerticalScrollIndicator: true,
      showHorizontalScrollIndicator: true,
      height: 550,
      width: 600
    });
    
   var art1 = controller.previewArticulo();
   scrollView.add(art1);
    
    rightHeaderView.add(rightTitleLbl);
    
    rightView.add(rightHeaderView);
    rightView.add(scrollView);
    
    
window.add(leftView);
window.add(rightView);
return window;
    
}

module.exports = main;