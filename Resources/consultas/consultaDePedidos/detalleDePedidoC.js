
exports.botonHeader=function(title){
    
    var boton = Ti.UI.createView({
        width:100,
        height:40,
        backgroundColor:'gray',
        borderWidth:1,
        action:title
    }); 
    
    var titleLbl= Ti.UI.createLabel({
        text:title,
        color:'white',
        touchEnabled:false
    });
    boton.add(titleLbl);
    
    boton.prender=function(mode){
        if(mode){
          boton.backgroundColor='navy';  
        }else{
            boton.backgroundColor='gray';   
        }
    };
    
    return boton;
};

exports.dgRow = function(title,value){
    
    var row = Ti.UI.createView({
        height:38,
        borderWidth:1,
        width:698
    });
    
    var titleLbl = Ti.UI.createLabel({
        text:title,
        left:1,
        width:'30%',
        textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT
    });
    var valueLbl = Ti.UI.createLabel({
        text:value,
        right:1,
        width:'69%',
        textAlign:Ti.UI.TEXT_ALIGNMENT_RIGHT
    });
    
    row.add(titleLbl);
    row.add(valueLbl);
    return row;
    
};

exports.comentRow = function(data){
  
  var row = Ti.UI.createView({
      top:15,
      height:Ti.UI.SIZE,
      width:Ti.UI.FILL,
      layout:'vertical',
      backgroundColor:'white'
  });  
  
  var header = Ti.UI.createView({
      height:20,
      layout:'horizontal',
      width:Ti.UI.FILL
  });
  
  var nombre = Ti.UI.createLabel({
      text:data.nombre,
      height:16,
      top:2,
      left:4
  });
  var fecha = Ti.UI.createLabel({
      text:data.fecha,
      height:16,
      top:2,
      left:6
  });
  header.add(nombre);
  header.add(fecha);
  row.add(header);
  
  var msj = Ti.UI.createLabel({
      text:data.msj,
      height:Ti.UI.SIZE,
      width:Ti.UI.FILL,
      top:7,
      left:6
  });
  row.add(msj);
  
  
  return row;
    
};
