
function consultPed(params){
    
    var controller = require('consultas/consultaDePedidos/mainC');
    var detPedObj = require('consultas/consultaDePedidos/detalleDePedidoUI');
    
    var window = Ti.UI.createWindow({
        title:'Consulta de Pedidos',
        backgroundColor:'white'
    });
    
 ////////////////////////////////////
 ////       Left  Side      ///////
 
 var leftView = Ti.UI.createView({
     left:0,
     height:'100%',
     width:'33%',
     backgroundColor:'white',
     layout:'vertical',
     borderWidth:2
 });
 
 var leftTitle = Ti.UI.createLabel({
     text:'Filtrar Pedidos Por:',
     textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
     width:'100%',
     font:{fontSize:21},
     backgroundColor:'gray'
 });
 
 var cptextField = Ti.UI.createTextField({
  hintText: ' Código de Pedido',
  left: 0,
  width: '100%', 
  height: 38
});
var cctextField = Ti.UI.createTextField({
  hintText: ' Código de Cliente',
  left: 0,
  width: '100%',
  borderWidth:1,
  height: 38
});
var nctextField = Ti.UI.createTextField({
  hintText: ' Nombre del Cliente',
  left: 0,
  width: '100%', 
  height: 38
});

var fpLabel = Ti.UI.createLabel({
    text:' Fecha de Pedido',
    backgroundColor:'gray',
    font:{fontSize:23},
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    width:'100%',
});
var fpDeLabel = Ti.UI.createLabel({
    text:' De',
    font:{fontSize:20},
    height:37,
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    width:'100%'
});
controller.pickerFecha(fpDeLabel);
var fpALabel = Ti.UI.createLabel({
    text:' A',
    backgroundColor:'#cccccc',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    font:{fontSize:20},
    height:37,
    width:'100%'
});
controller.pickerFecha(fpALabel);

var feLabel = Ti.UI.createLabel({
    text:' Fecha de Entrega',
    backgroundColor:'gray',
    font:{fontSize:23},
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    width:'100%',
});
var feDeLabel = Ti.UI.createLabel({
    text:' De',
    font:{fontSize:20},
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    width:'100%',
    height:37
});
controller.pickerFecha(feDeLabel);
var feALabel = Ti.UI.createLabel({
    text:' A',
    backgroundColor:'#cccccc',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    font:{fontSize:20},
    height:37,
    width:'100%'
});
controller.pickerFecha(feALabel);

var estatus = controller.tipo('Estatus');

var buttonAccept = Ti.UI.createButton({
    title:'Buscar',
    top:30
});

leftView.add(leftTitle);
leftView.add(cptextField);
leftView.add(cctextField);
leftView.add(nctextField);
leftView.add(fpLabel);
leftView.add(fpDeLabel);
leftView.add(fpALabel);
leftView.add(feLabel);
leftView.add(feDeLabel);
leftView.add(feALabel);
leftView.add(estatus);
leftView.add(buttonAccept);


///////////////////////////////////////////////////////////////
///////     Right   View    /////////////

var rightView = Ti.UI.createView({
    width:'67%',
    height:'100%',
    right:0,
    top:0
});

var rightTitle = Ti.UI.createLabel({
    text:'Pedidos',
    font:{fontSize:22, fontWeight:'bold'},
    top:0
});
var buttonDP = Ti.UI.createButton({
    title:'Detalle Pedido',
    top:130
});

buttonDP.addEventListener('click',function(){
    
   var dpw = new detPedObj();
   params.openW(dpw);
    
});


rightView.add(rightTitle);
rightView.add(buttonDP);

window.add(leftView);
window.add(rightView);
    
    return window;
}

module.exports = consultPed;
