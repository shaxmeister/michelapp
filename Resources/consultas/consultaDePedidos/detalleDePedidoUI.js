
/*
 * Ventana de detalle de pedido
 */

function ddp(){
    
    var controller = require('consultas/consultaDePedidos/detalleDePedidoC');
    
    var window = Ti.UI.createWindow({
        title:'Detalle de Pedido',
        backgroundColor:'white'
    });

////////////////////////////////////////////
////////        Header          ///////////
    
    var header = Ti.UI.createView({
        backgroundColor:'silver',
        top:0,
        width:'100%',
        borderWidth:2,
        height:50
    });
    var headerButonsHolder = Ti.UI.createView({
        layout:'horizontal',
        width:Ti.UI.SIZE,
        height:40,
        top:5
    });
    
    var dgButton = controller.botonHeader('Datos Generales');
    dgButton.prender(true);
    var paButton = controller.botonHeader('Por Artículo');
    var comButton = controller.botonHeader('Comentarios');
    var ofButton = controller.botonHeader('Órdenes/Facturas');
    
    headerButonsHolder.add(dgButton);
    headerButonsHolder.add(paButton);
    headerButonsHolder.add(comButton);
    headerButonsHolder.add(ofButton);
    
    headerButonsHolder.addEventListener('click',function(e){
        Ti.API.info(JSON.stringify(e));
        dgButton.prender(false);
    paButton.prender(false);
    comButton.prender(false);
    ofButton.prender(false);
    e.source.prender(true);
        switch(e.source.action){
            
            case 'Datos Generales':
            viewDG.show();
            viewPA.hide();
            viewCO.hide();
            break;
            case 'Por Artículo':
            viewDG.hide();
            viewPA.show();
            viewCO.hide();
            break;
            case 'Comentarios':
            viewDG.hide();
            viewPA.hide();
            viewCO.show();
            break;
            case 'Órdenes/Facturas':
            
            break;
            
        }
        
    });
    
    
    header.add(headerButonsHolder);
/////////////////////////////////////////////////////////////

/////////          Datos Generales          //////////

var viewDG = Ti.UI.createView({
    width:'100%',
    height:650,
    top:50,
    backgroundColor:'lightgray',
    layout:'vertical'
});

var titleLbl = Ti.UI.createLabel({
    text:'Datos Generales',
    font:{fontSize:22,fontWeight:'bold'},
    top:20,
    left:40
});

var dgBlock = Ti.UI.createView({
    height:Ti.UI.SIZE,
    width:700,
    top:30,
    borderWidth:2,
    backgroundColor:'white',
    borderRadius:9,
    layout:'vertical'
});

var rowCC = controller.dgRow('Código de Cliente','329632o723');
var rowNC = controller.dgRow('Nombre de Cliente','Juan Perez Perez');
var rowCP = controller.dgRow('Código de Pedido','329632o723');
var rowFGP = controller.dgRow('Fecha de Generacion de Pedido','liuhluis sauhciwule eiu');
var rowMT = controller.dgRow('Monto Total','55555555');
var rowTT = controller.dgRow('Trailer/Torton','329632o723');

dgBlock.add(rowCC);
dgBlock.add(rowNC);
dgBlock.add(rowCP);
dgBlock.add(rowFGP);
dgBlock.add(rowMT);
dgBlock.add(rowTT);

viewDG.add(titleLbl);
viewDG.add(dgBlock);


/////////////////////////////////////////////////////////////

/////////          Por Articulo          //////////
    var viewPA = Ti.UI.createView({
    width:'100%',
    height:650,
    top:50,
    backgroundColor:'lightgray',
    layout:'vertical'
});
viewPA.hide();

/////////////////////////////////////////////////////////////

/////////          Comentarios          //////////
    var viewCO = Ti.UI.createView({
    width:'100%',
    height:650,
    top:50,
    backgroundColor:'lightgray',
    layout:'vertical',
    borderWidth:2
});
    
    var comentsScroll = Ti.UI.createScrollView({
        width:Ti.UI.FILL,
        height:650,
        contentWidth: 'auto',
  contentHeight: 'auto'
    });
    
    var data = {
        msj:'bla bla bla ñoshcisdj.',
        fecha:'14-febrero-2013  12:30am',
        nombre:'Daniela Alatorre'
    }
    
    comentsScroll.add(controller.comentRow(data));
    
    viewCO.add(comentsScroll);
    viewCO.hide();
    /////////////////////////////////////////////////////////////

/////////          orden Factura          //////////
   var viewFA = Ti.UI.createView({
    width:'100%',
    height:650,
    top:50,
    backgroundColor:'lightgray',
    layout:'vertical'
});
viewFA.hide();


    
    window.add(header);
    window.add(viewDG);
    window.add(viewPA);
    window.add(viewCO);
    window.add(viewFA);
    
    return window;
}

module.exports = ddp;