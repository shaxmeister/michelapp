

function main(params){
    
    var controller = require('consultas/analisisVenta/controller');
    
    var window = Ti.UI.createWindow({
    title:'Análisis de Venta',
    backgroundColor:'white',
    layout:'composite'
});
    
    var filtrosBlock = Ti.UI.createView({
    height:Ti.UI.SIZE,
    width:550,
    top:100,
    left:100,
    borderWidth:2,
    layout:'vertical'
    });
    
    var compararRow = controller.rowFiltro({
        title:'Comparar',
        popover:false,
    });
    compararRow.backgroundColor='lightgray';
    var compararAnoRow = controller.rowFiltro({
        title:' Fecha',
        popover:false,
    });
    controller.pickerFecha(compararAnoRow);
    var compararContraRow = controller.rowFiltro({
        title:'Contra',
        popover:false,
    });
    compararContraRow.backgroundColor='lightgray';
    var compararContraAnoRow = controller.rowFiltro({
        title:' Fecha',
        popover:false,
    });
    controller.pickerFecha(compararContraAnoRow);
    var grupRow = controller.rowFiltro({
        title:'Grupo',
        popover:true,
        value:[{'title':'Por Estado'},{'title':'Por Línea'},{'title':'Por País'}]
    });
    var valoresRow = controller.rowFiltro({
        title:'Valores',
        popover:true,
        value:[{'title':'Unidades'},{'title':'Peso Neto'},{'title':'Importe Pesos s/iva'},{'title':'Importe USD s/iva'}]
    });
    var desRow = controller.rowFiltro({
        title:' Desarrolador de Mercado',
        popover:false,
    });
    
    filtrosBlock.add(compararRow);
    filtrosBlock.add(compararAnoRow);
    filtrosBlock.add(compararContraRow);
    filtrosBlock.add(compararContraAnoRow);
    filtrosBlock.add(grupRow);
    filtrosBlock.add(valoresRow);
    filtrosBlock.add(desRow);

    var aceptarButton = Ti.UI.createButton({
    bottom:70,
    width:120,
    height:38,
    title:'Confirmar'
});
    
    window.add(filtrosBlock);
    window.add(aceptarButton);
    return window;
}

module.exports = main;