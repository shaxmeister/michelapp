
exports.rowFiltro = function(params){
    
    var row = Ti.UI.createView({
    height:38,
    width:600,
    top:0,
    left:0,
    borderWidth:1
    });
    
    var titleLbl = Ti.UI.createLabel({
    text:params.title,
    font:{fontSize:13 ,fontWeight:'regular'},
    height:36,
    width:Ti.UI.FILL,
    top:1,
    color:'black',
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    left:0 
    });
    
    if(params.popover){
     var tableData = params.value;

    var table = Ti.UI.createTableView({
    data: tableData
    });
    table.addEventListener('click',function(e){
        //Ti.API.info("listener table "+JSON.stringify(e.row.title));
        titleLbl.setText(e.row.title);
    });
    
  var  popover = Ti.UI.iPad.createPopover({ 
        width:300, 
        height:250,
        title:params.title,
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    popover.add(table);
    popover.addEventListener('click',function(e){
        popover.hide();
    });
     titleLbl.addEventListener('click',function(){
    popover.show({
        view:titleLbl,
        animated:true
    }); 
    });
    }
    row.add(titleLbl);
    row.setText= function(txt){
        titleLbl.setText(txt);
    };
    
    return row;
};


///// Picker Fecha  ////
exports.pickerFecha = function(origin){
    
    var picker = Ti.UI.createPicker();
    var yearRange={min: 1990, max: 2030};
    var monthRange={min: 1, max: 12};
    var yCol = Ti.UI.createPickerColumn();
    for(var i = yearRange.min; i <= yearRange.max; i++) {
        yCol.addRow(Ti.UI.createPickerRow({
            title : i.toString()
        }));
    }
 
    var mCol = Ti.UI.createPickerColumn();
    for(var i = monthRange.min; i <= monthRange.max; i++) {
        mCol.addRow(Ti.UI.createPickerRow({
            title : i.toString()
        }));
    }
 
    picker.add([yCol, mCol]);
    
picker.addEventListener('change',function(e){
    Ti.API.info(JSON.stringify(e));
  origin.setText("  "+e.selectedValue[1]+", "+e.selectedValue[0]);
});

var  popover = Ti.UI.iPad.createPopover({ 
        width:300, 
        height:250,
        title:"Escoge",
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    popover.addEventListener('click',function(e){
        popover.hide();
    });
    popover.add(picker);
     origin.addEventListener('click',function(){
    popover.show({
        view:origin,
        animated:true
    }); 
    
    });


    
};
