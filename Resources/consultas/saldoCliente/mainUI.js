
function main(){
    
    var window = Ti.UI.createWindow({
    title:'Saldo de Cliente',
    backgroundColor:'white',
    layout:'composite'
    });
    
    var webview = Titanium.UI.createWebView({
        width:400,
        height:400
    });
    
    var tsLbl = Ti.UI.createLabel({
    text:'Total saldo',
    font:{fontSize:16 ,fontWeight:'bold'},
    height:20,
    width:Ti.UI.SIZE,
    bottom:4,
    left:200 
    });
    var tcLbl = Ti.UI.createLabel({
    text:'Total TC',
    font:{fontSize:16 ,fontWeight:'bold'},
    height:20,
    width:Ti.UI.SIZE,
    bottom:4,
    right:200 
    });
    
    window.add(tsLbl);
    window.add(tcLbl);
    
    return window;   
}

module.exports=main;
