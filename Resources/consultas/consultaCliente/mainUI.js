
function main(){
    
    var controllers = require('consultas/consultaCliente/controller');
    
   var window = Ti.UI.createWindow({
    title:'Consulta de Clientes',
    backgroundColor:'white',
    layout:'vertical'
    });
    
    var filtroBlockView = Ti.UI.createView({
    height:Ti.UI.SIZE,
    width:600,
    top:20,
    layout:'vertical',
    backgroundColor:'gray'
    });
    
    var filtrosTitleLbl = Ti.UI.createLabel({
    text:'Filtrar Cliente Por',
    font:{fontSize:20 ,fontWeight:'bold'},
    height:30,
    width:Ti.UI.SIZE,
    top:0,
    left:5 
    });
    
    var cc = controllers.rowFiltro({
        title:'Código del Cliente',
        value:'XOXOXXX'
    });
    var nc = controllers.rowFiltro({
        title:'Nombre del Cliente',
        value:'sfa agrfaerg gergergr ergdsscds.'
    });
    var tipo = controllers.rowFiltro({
        title:'Tipo',
        value:[ {title: 'Todos'},{title: 'AutoServicio'},{title: 'Exportación Extranjero'},{title: 'Exportación Nacional'},{title: 'Gobierno'},{title: 'Mayoristas'},{title: 'Proveedores'}]
    });
    var giro = controllers.rowFiltro({
        title:'Giro',
        value:[ {title: 'Todos'},{title: 'Abarrotero'},{title: 'AutoServicio'},{title: 'Dulcero'},{title: 'Freidor'},{title: 'Gobierno'},{title: 'Juguetería'},{title: 'Mercería y Papeleria'},{title: 'Materias Primas'},{title: 'Proveedor'},{title: 'Semillas'}]
    });
    var rfc = controllers.rowFiltro({
        title:'RFC',
        value:'rrrfffcc'
    });
    
    var okButton = Ti.UI.createButton({
    top:20,
    right:0,
    width:120,
    height:38,
    title:'aceptar'
    });
    
    filtroBlockView.add(filtrosTitleLbl);
    filtroBlockView.add(cc);
    filtroBlockView.add(nc);
    filtroBlockView.add(tipo);
    filtroBlockView.add(giro);
    filtroBlockView.add(rfc);
    filtroBlockView.add(okButton);
    
    var resultadosTitleLbl = Ti.UI.createLabel({
    text:'Clientes',
    font:{fontSize:22 ,fontWeight:'bold'},
    height:22,
    color:'blue',
    width:Ti.UI.SIZE,
    top:30,
    });
    
    

    window.add(filtroBlockView);
    window.add(resultadosTitleLbl);
    
    return window;
}

module.exports = main;
