
exports.rowFiltro = function(params){
    
   var row = Ti.UI.createView({
    height:38,
    width:500,
    top:0,
    borderWidth:1,
    backgroundColor:'white',
    layout:'horizontal'
});

var titleLbl = Ti.UI.createLabel({
    text:params.title,
    font:{fontSize:13 ,fontWeight:'regular'},
    height:38,
    width:150,
    top:0,
    left:0 
    }); 
    
    if( typeof params.value === 'string' ) {
    
var valueLbl = Ti.UI.createLabel({
    text:params.value,
    font:{fontSize:13 ,fontWeight:'regular'},
    height:38,
    textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
    width:350,
    paddingLeft:3,
    top:0,
    right:0 
    }); 
    
    }else{
        
         var valueLbl = Ti.UI.createLabel({
        width: 350, height: 38,
        text:' Seleccione una opción             ...',
        paddingLeft:3,
        textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
        right:0
    });   
    
    var tableData = params.value;

    var table = Ti.UI.createTableView({
    data: tableData
    });
    table.addEventListener('click',function(e){
        //Ti.API.info("listener table "+JSON.stringify(e.row.title));
        valueLbl.setText(e.row.title);
    });
    
  var  popover = Ti.UI.iPad.createPopover({ 
        width:300, 
        height:250,
        title:params.title,
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    popover.add(table);
    popover.addEventListener('click',function(e){
        popover.hide();
    });
     valueLbl.addEventListener('click',function(){
    popover.show({
        view:valueLbl,
        animated:true
    }); 
    
    });
    
        
    }

    
    row.add(titleLbl);
    row.add(valueLbl);
    
    return row;
    
};
