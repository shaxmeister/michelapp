/*
 * Ventana principal parte UI
 */


/*
 * CAAMBIAARR LOS LISTENEERS POR UNO SOLO!!!
 */


function principalUI(params){
    
    var admPedObj="",conPedObj="",comIntObj="",devObj="",captCredCobraObj="",encuestasObj="",invCliObj="";
    var consPedObj="",recMsjObj="",publPromObj="", salClienteObj="", consClienteObj="", consAnaVenObj="",credCobrObj="",articulosObj="";
    var ventasPorAgntObj="", ventasLinProdObj="", ventasXLinProdObj="";
    
    var controllers = require('principal/principalC');
    var utils = require('utilities');
    var window = Ti.UI.createWindow({
        backgroundColor:"white",
        title:"Principal"
    });
    
   //////////////////////////// 
///////        Header      ////////

    var headerHolder = Ti.UI.createView({
        width: utils.width,
        height: 80,
        top:0,
        layout:"horizontal"
    });    
    var logo = Ti.UI.createView({
        width:155,
        height:70,
        left:9,
        top:5,
        backgroundImage:'images/Michel.png'
    });
    
    var procesosButton = Ti.UI.createButton({
        width:110,
        height:60,
        top:10,
        left:300,
        backgroundImage:"",
        layout:'vertical'
    });
    var procImg = Ti.UI.createView({
        width:46,
        height:44,
        backgroundImage:'images/principal/Procesos.png'
    });
    var procTitle = Ti.UI.createLabel({
        text:'Procesos',
        height:16,
        top:1,
        color:'#094FA4'
    });
    procesosButton.add(procImg);
    procesosButton.add(procTitle);
    var consultasButton = Ti.UI.createButton({
        width:110,
        height:60,
        left:4,
        top:10,
        backgroundImage:"",
        layout:'vertical'
    });
    var procImg = Ti.UI.createView({
        width:46,
        height:44,
        backgroundImage:'images/principal/Consulta.png'
    });
    var procTitle = Ti.UI.createLabel({
        text:'Consultas',
        height:16,
        top:1,
        color:'#094FA4'
    });
    consultasButton.add(procImg);
    consultasButton.add(procTitle);
    
    ////  BUTTONS LISTENERS     /////
    
    procesosButton.addEventListener('click',function(e){
        scrollView.show();
        scrollViewConsultas.hide();
    });
    consultasButton.addEventListener('click',function(e){
        scrollView.hide();
        scrollViewConsultas.show();
    });
   
    
    
    headerHolder.add(logo);
    headerHolder.add(procesosButton);
    headerHolder.add(consultasButton);
    
   //////////////////////////// 
///////        Opciones Scrollable view      ////////
    var admPedidosview1 = Ti.UI.createView({ backgroundImage:'images/principal/AdmPedidos.png' ,width:116 ,height:116, borderRadius:10,left:20});
    admPedidosview1.addEventListener('click',function(e){
        if(admPedObj=="")admPedObj=require('procesos/admPedidos/main');
        var admPed = new admPedObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(admPed);
    });
    
var capInfoCréditoyCobranzaview2 = Ti.UI.createView({ backgroundImage:'images/principal/CapInfoCreditoyCobranza.png' ,width:116 ,height:116, borderRadius:10,left:20});
capInfoCréditoyCobranzaview2.addEventListener('click',function(e){
    if(captCredCobraObj=="")captCredCobraObj=require('procesos/CapInfoCreditoyCobranza/mainUI');
        var captCredCobra = new captCredCobraObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(captCredCobra);
    
});


var comInternaview3 = Ti.UI.createView({ backgroundImage:'images/principal/ComInterna.png',width:116 ,height:116,borderRadius:10, left:20});
comInternaview3.addEventListener('click',function(e){
    if(comIntObj=="")comIntObj=require('procesos/comunInterna/mainUI');
        var comInt = new comIntObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(comInt);
    
});

var confPedidosview4 = Ti.UI.createView({ backgroundImage:'images/principal/ConfPedidos.png',width:116 ,height:116,borderRadius:10, left:20});
confPedidosview4.addEventListener('click',function(e){
        if(conPedObj=="")conPedObj=require('procesos/confirmarPedido/pedidosXConfUI');
        var conPed = new conPedObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(conPed);
    });

var devolucionesview5 = Ti.UI.createView({ backgroundImage:'images/principal/Devoluciones.png' ,width:116,height:116, borderRadius:10,left:20});
devolucionesview5.addEventListener('click',function(e){
        if(devObj=="")devObj=require('procesos/devoluciones/devolucionesUI');
        var dev = new devObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(dev);
    });


var encuestasview = Ti.UI.createView({ backgroundImage:'images/principal/Encuestas.png' ,width:116,height:116,borderRadius:10, left:20});

encuestasview.addEventListener('click',function(e){
        if(encuestasObj=="")encuestasObj=require('procesos/encuestas/encuestasUI');
        var encuestasW = new encuestasObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(encuestasW);
    });

var invClienteview = Ti.UI.createView({ backgroundImage:'images/principal/InvCliente.png' ,width:116, height:116,borderRadius:10,left:20});
invClienteview.addEventListener('click',function(e){
        if(invCliObj=="")invCliObj=require('procesos/inventarioCliente/mainUI');
        var invCli = new invCliObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(invCli);
    });

var regNoVentaview = Ti.UI.createView({ backgroundImage:'images/principal/RegNoVenta.png' ,width:116,height:116, borderRadius:10,left:20});

var scrollHolder = Ti.UI.createView({
    height:146,
    width:Ti.UI.FILL,
    backgroundImage:'images/principal/ScrollG.png',
    top:80
});

var scrollView = Ti.UI.createScrollView({
  backgroundColor:'white',
  layout:'horizontal',
  height:126,
  borderRadius:10,
  width: (utils.width-60),
  top:10,
  verticalBounce:false,
  left:20,
  contentWidth: 'auto',
  contentHeight: 110,
});
scrollView.add(admPedidosview1);
scrollView.add(capInfoCréditoyCobranzaview2);
scrollView.add(comInternaview3);
scrollView.add(confPedidosview4);
scrollView.add(devolucionesview5);
scrollView.add(encuestasview);
scrollView.add(invClienteview);
scrollView.add(regNoVentaview);


var scrollViewConsultas = Ti.UI.createScrollView({
  backgroundColor:'white',
  layout:'horizontal',
  height:126,
  borderRadius:10,
  width: (utils.width-60),
  top:10,
  verticalBounce:false,
  left:20,
  contentWidth: 'auto',
  contentHeight: 110,
});
var AnalisisVenta = Ti.UI.createView({ backgroundImage:'images/principal/AnalisisVenta.png' ,width:116 ,height:116, borderRadius:10,left:20});
AnalisisVenta.addEventListener('click',function(e){
        if(consAnaVenObj=="")consAnaVenObj=require('consultas/analisisVenta/mainUI');
        var consAnaVen = new consAnaVenObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(consAnaVen);
    });
var ConsArticulos = Ti.UI.createView({ backgroundImage:'images/principal/ConsArticulos.png',width:116 ,height:116,borderRadius:10, left:20});

ConsArticulos.addEventListener('click',function(e){
        if(articulosObj=="")articulosObj=require('consultas/deArticulos/mainUI');
        var articulos = new articulosObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(articulos);
    });
var ConsClientes = Ti.UI.createView({ backgroundImage:'images/principal/ConsClientes.png',width:116 ,height:116,borderRadius:10, left:20});
ConsClientes.addEventListener('click',function(e){
        if(consClienteObj=="")consClienteObj=require('consultas/consultaCliente/mainUI');
        var consCliente = new consClienteObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(consCliente);
    });

var ConsPedidos = Ti.UI.createView({ backgroundImage:'images/principal/ConsPedidos.png' ,width:116,height:116, borderRadius:10,left:20});
ConsPedidos.addEventListener('click',function(e){
        if(consPedObj=="")consPedObj=require('consultas/consultaDePedidos/mainUI');
        var consPed = new consPedObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(consPed);
    });

var CreditoyCobranza = Ti.UI.createView({ backgroundImage:'images/principal/CreditoyCobranza.png' ,width:116,height:116,borderRadius:10, left:20});
CreditoyCobranza.addEventListener('click',function(e){
        if(credCobrObj=="")credCobrObj=require('consultas/credYCobranza/mainUI');
        var credCobr = new credCobrObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(credCobr);
    });

var PublicidadPromocion = Ti.UI.createView({ backgroundImage:'images/principal/Publicidad-Promocion.png' ,width:116, height:116,borderRadius:10,left:20});
PublicidadPromocion.addEventListener('click',function(e){
        if(publPromObj=="")publPromObj=require('consultas/publicidadPromociones/mainUI');
        var publProm = new publPromObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(publProm);
    });

var RecepMensajes = Ti.UI.createView({ backgroundImage:'images/principal/RecepMensajes.png' ,width:116,height:116, borderRadius:10,left:20});
RecepMensajes.addEventListener('click',function(e){
        if(recMsjObj=="")recMsjObj=require('consultas/recepcionMensajes/recMsjs');
        var recMsj = new recMsjObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(recMsj);
    });

var SalCliente = Ti.UI.createView({ backgroundImage:'images/principal/SalCliente.png' ,width:116,height:116, borderRadius:10,left:20});
SalCliente.addEventListener('click',function(e){
        if(salClienteObj=="")salClienteObj=require('consultas/saldoCliente/mainUI');
        var salCliente = new salClienteObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(salCliente);
    });


var VenAgente = Ti.UI.createView({ backgroundImage:'images/principal/VenAgente.png' ,width:116,height:116, borderRadius:10,left:20});
VenAgente.addEventListener('click',function(e){
        if(ventasPorAgntObj=="")ventasPorAgntObj=require('consultas/ventasPorAgente/mainUI');
        var ventasPorAgnt = new ventasPorAgntObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(ventasPorAgnt);
    });
var VentaLineaProd = Ti.UI.createView({ backgroundImage:'images/principal/VentaLineaProd.png' ,width:116,height:116, borderRadius:10,left:20});
VentaLineaProd.addEventListener('click',function(e){
        if(ventasLinProdObj=="")ventasLinProdObj=require('consultas/ventasLineaProductoDetalle/mainUI');
        var ventasLinProd = new ventasLinProdObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(ventasLinProd);
    });



var VentasXLineaProducto = Ti.UI.createView({ backgroundImage:'images/principal/VentasXLineaProducto.png' ,width:116,height:116, borderRadius:10,left:20});
VentasXLineaProducto.addEventListener('click',function(e){
        if(ventasXLinProdObj=="")ventasXLinProdObj=require('consultas/ventasLineaProductoDetalle/ventaLineaPorLinea');
        var ventasXLinProd = new ventasXLinProdObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(ventasXLinProd);
    });

scrollViewConsultas.add(AnalisisVenta);
scrollViewConsultas.add(ConsArticulos);
scrollViewConsultas.add(ConsClientes);
scrollViewConsultas.add(ConsPedidos);
scrollViewConsultas.add(CreditoyCobranza);
scrollViewConsultas.add(PublicidadPromocion);
scrollViewConsultas.add(RecepMensajes);
scrollViewConsultas.add(SalCliente);
scrollViewConsultas.add(VenAgente);
scrollViewConsultas.add(VentaLineaProd);
scrollViewConsultas.add(VentasXLineaProducto);

scrollViewConsultas.hide();
scrollHolder.add(scrollViewConsultas);
scrollHolder.add(scrollView);
//////////////////////////// 
///////       Body     ////////

var leftBody = Ti.UI.createView({
    width:(utils.width/3)-70,
    height:500,
    layout:'vertical',
    top:231,
    left:10
});

var headerLeftBody = Ti.UI.createView({
     width:(utils.width/3)-90,
    height:46,
    layout:'horizontal',
    top:10
});

var headerImg = Ti.UI.createView({
    backgroundImage:'images/principal/InfoGeneral.png',
    height:40,
    width:40,
    top:3
});

var infoLblTitle = Ti.UI.createLabel({
    text:'Información General',
    width:Ti.UI.SIZE,
    left:5,
    color:'#094FA4',
    font: { fontSize:19, fontWeight:'bold'},
    top:3
});
headerLeftBody.add(infoLblTitle);
headerLeftBody.add(headerImg);

var infoSopLbl = Ti.UI.createLabel({
    text:'Información de Soporte',
    width:Ti.UI.FILL,
    font: { fontSize:17 },
    left:5,
    top:25
});
var infoSopButton = Ti.UI.createButton({
    title:'Información de Soporte',
    width:Ti.UI.FILL,
    height:17,
    left:5,
    top:10
});

var servLbl = Ti.UI.createLabel({
    text:'Servidor',
    width:Ti.UI.FILL,
    left:5,
    font: { fontSize:17 },
    top:10
});
var visitaRLbl = Ti.UI.createLabel({
    text:'Visitas realizadas',
    width:Ti.UI.FILL,
    font: { fontSize:17 },
    left:5,
    top:2
});
var visitaALbl = Ti.UI.createLabel({
    text:'Visitas Acumuladas',
    width:Ti.UI.FILL,
    font: { fontSize:17 },
    left:5,
    top:2
});
var tvLbl = Ti.UI.createLabel({
    text:'Total de Ventas',
    width:Ti.UI.FILL,
    font: { fontSize:17 },
    left:5,
    top:2
});
var ovLbl = Ti.UI.createLabel({
    text:'Objetivo de Visita',
    width:Ti.UI.FILL,
    font: { fontSize:17 },
    left:5,
    top:2
});



var midBody = Ti.UI.createView({
    width:(utils.width/3)+100,
    height:380,
    top:261,
    borderRadius:9,
    left:(utils.width/3)-50,
    backgroundColor:'black'
});
var midBodyTableHolder = Ti.UI.createView({
    width:(utils.width/3)+90,
    height:320,
    top:50,
    left:5,
    backgroundColor:'gray'
});
var title = Ti.UI.createLabel({
    text:'Notificaciones',
    width:Ti.UI.FILL,
    font: { fontSize:18 ,fontWeight:'bold'},
    height:14,
    color:'white',
    top:20,
    left:150
});

var notifTv=controllers.notifTable();
midBodyTableHolder.add(notifTv);
midBody.add(title);
midBody.add(midBodyTableHolder);
    leftBody.add(headerLeftBody);
    leftBody.add(infoSopLbl);
    leftBody.add(infoSopButton);
    leftBody.add(servLbl);
    leftBody.add(visitaRLbl);
    leftBody.add(visitaALbl);
    leftBody.add(tvLbl);
    leftBody.add(ovLbl);

var rightBody = Ti.UI.createView({
    width:(utils.width/3)-50,
    height:460,
    top:231,
    layout:'vertical',
    right:0
});
var headerRightBody = Ti.UI.createView({
     width:(utils.width/3)-90,
    height:46,
    layout:'horizontal',
    top:10
});

var headerRightImg = Ti.UI.createView({
    backgroundImage:'images/principal/DatosCliente.png',
    height:40,
    width:40,
    left:20,
    top:3
});
var rightBodyTitle = Ti.UI.createLabel({
    text:'Datos del Cliente',
    width:Ti.UI.SIZE,
   font: { fontSize:19, fontWeight:'bold'},
    height:Ti.UI.SIZE,
    color:'#094FA4',
    top:10,
    left:0
});
headerRightBody.add(rightBodyTitle);
headerRightBody.add(headerRightImg);

var rightBodyInfoHolder = Ti.UI.createView({
    width:(utils.width/3)-100,
    layout:'vertical',
    top:10,
    height:355,
    borderWidth:2,
    borderColor:'gray',
    borderRadius:9
});
rightBody.add(headerRightBody);
controllers.fillDatosCliente(rightBodyInfoHolder);
rightBody.add(rightBodyInfoHolder);



    
    window.add(headerHolder);
    window.add(scrollHolder);
    window.add(leftBody);
    window.add(midBody);
    window.add(rightBody);
    
    return window;
}

module.exports = principalUI;
