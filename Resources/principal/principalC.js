


exports.notifTable = function(){
  
  var table = Ti.UI.createTableView();  
   
  return table;
};

exports.fillDatosCliente = function(view){
    
    var arr = [' Código del cliente',' Nombre del Cliente',' Razón Social',' RFC',' Domicilio',' Fecha de Visita',' Hora de inicio de Visita'];
    
    for(x in arr){
        
        var row = Ti.UI.createView({
            height:51,
            borderWidth:1
        });
        var info = Ti.UI.createLabel({
            text: arr[x],
            left:0,
            font: {fontSize:13}
        });
        row.add(info);
       view.add(row);
    }
    
};
