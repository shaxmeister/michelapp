
/*
 Script principal de Michel.
 */

//bootstrap and check dependencies
if (Ti.version < 1.8 ) {
    alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');       
}

// This is a single context application with multiple windows in a stack
(function() {
    //render appropriate components based on the platform and form factor
    var utils = require('utilities');
    //Ti.API.info('osname: '+utils.osname);  
    //Ti.API.info('height: '+ utils.height);   
    //Ti.API.info('width: '+ utils.width);     
        
    
    
    var loginWinObj = require('login/loginUI');
    var loginWin = loginWinObj();
    loginWin.open();

})();
