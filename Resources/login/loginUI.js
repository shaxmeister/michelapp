/*
 * Ventana de login parte UI
 */

function loginUI(){
    
    var controllers = require('login/loginC');
    var utils = require('utilities');
    var window = Ti.UI.createWindow({
        backgroundColor:"white"
    });

/////       Logo principal imagen   ////   
    var logo = Ti.UI.createView({
        width:801 ,
        height:226 ,
        backgroundImage:'images/Michel.png',
        top: (utils.height/6)
    });
 
 /////      TEXTFIELDS      /////   
    var usuarioTF = Ti.UI.createTextField({
      borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
      color: 'black',
      bottom: (utils.height/5)+51,
      hintText:"Usuario",
      width: (utils.width*0.85), height: 50
    });
    var contraTF = Ti.UI.createTextField({
      borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
      color: 'black',
      bottom: (utils.height/5),
      hintText:"Contraseña",
      passwordMask:true,
      width: (utils.width*0.85), height: 50
    });
    
    var botonLogin = Ti.UI.createButton({
        title:"Login",
        width:120,
        height:60,
        bottom:(utils.height/9)
    });
    botonLogin.addEventListener('singletap',function(){
        controllers.login(usuarioTF.value,contraTF.value);
        }); 
    
    window.add(usuarioTF);
    window.add(contraTF);
    window.add(botonLogin);
    window.add(logo);
    
    return window;
}

module.exports = loginUI;
