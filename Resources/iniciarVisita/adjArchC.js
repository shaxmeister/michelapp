
exports.rowAdj = function(settings){
    
    var mainView = Ti.UI.createView({
        width:Ti.UI.FILL,
        height:36,
        borderWidth:1
    });
    
    var titleLbl = Ti.UI.createLabel({
        text:settings.title,
        width:'60%',
        font:{fontSize:18},
        left:2
    });
    
    var icon = Ti.UI.createImageView({
        image:'images/appicon.png',
        width:30,
        height:30,
        right:4
    });
    
    mainView.add(titleLbl);
    mainView.add(icon);
    
    return mainView;
    
};
