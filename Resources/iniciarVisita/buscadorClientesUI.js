
function buscadorClientes(){
    
    var utils = require('utilities');
    var controllers = require('iniciarVisita/buscadorClientesC');
    
    var window = Ti.UI.createWindow({
        title:'Buscador de Clientes',
        backgroundColor:"white",
        layout:'vertical'
    });
    
    ///////      VIEW  Top       ////////
    var topView = Ti.UI.createView({
    width:'90%',
    backgroundColor:'gray',
    layout:'vertical',
    top:20,
    height:400,
    borderRadius:10
    });
    var topViewRowHolder = Ti.UI.createView({
    width:'90%',
    layout:'vertical',
    top:20,
    height:Ti.UI.SIZE,
        borderRadius:10
    });
    var topViewTitlelbl=Ti.UI.createLabel({
        text:'Buscador Clientes',
        color:'white',
        left:15,
        font: {fontSize:18, fontWeight:'bold'},
        top:10
    });
    
    ///  Filtros  ////
    var codigoRow = controllers.rowFiltro({
        title:'Código de Cliente',
        tipo:'numeric'
    });
    var nombreRow = controllers.rowFiltro({
        title:'Nombre del Cliente',
        tipo:'text'
    });
    var desarrolladorRow = controllers.rowFiltro({
        title:'Desarrollador de mercado',
        tipo:'text',
        mode:1
    });
    var tipoRow = controllers.rowFiltro({
        title:'Tipo',
        tipo:'tipoCliente',
        mode:1
    });
    var retenerRow = controllers.rowFiltro({
        title:'Retener crédito',
        tipo:'tipoCliente',
        mode:2
    });
    var estatusRow = controllers.rowFiltro({
        title:'Estatus del cliente',
        tipo:'text',
    });
    var giroRow = controllers.rowFiltro({
        title:'Giro',
        tipo:'tipoCliente',
        mode:3
    });
    var rfcRow = controllers.rowFiltro({
        title:'RFC',
        tipo:'text'
    });
    
    
    topViewRowHolder.add(codigoRow);
    topViewRowHolder.add(nombreRow);
    topViewRowHolder.add(desarrolladorRow);
    topViewRowHolder.add(tipoRow);
    topViewRowHolder.add(retenerRow);
    topViewRowHolder.add(estatusRow);
    topViewRowHolder.add(giroRow);
    topViewRowHolder.add(rfcRow);
    
    topView.add(topViewTitlelbl);
    topView.add(topViewRowHolder);
    window.add(topView);
    
    
    ///////     View Bottom     /////
    
    var bottomTitlelbl=Ti.UI.createLabel({
        text:'Clientes',
        color:'blue',
        left:15,
        font: {fontSize:18, fontWeight:'bold'},
        top:20
    });
    window.add(bottomTitlelbl);
    
    return window;
}

module.exports=buscadorClientes;