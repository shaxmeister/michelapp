
function servicioUI(params){
    
    var controllers = require('iniciarVisita/buscadorClientesC');
    var adjArchObj = "";
   var guardarButton = Ti.UI.createButton({
       title:'Guardar'
   });
    
    var window = Ti.UI.createWindow({
        title:'Servicio',
        backgroundColor:'white',
        rightNavButton:guardarButton
    });
 
 //////      MAIN VIEW      /////   
    var mainView = Ti.UI.createView({
        height:Ti.UI.SIZE,
        width:'90%',
        borderWidth:2,
        borderRadius:9,
        top:15,
        layout:'vertical'
    });

    
    
    var nombreRow = controllers.rowFiltro({
        title:'Nombre del Cliente',
        tipo:'text'
    });
    var calleRow = controllers.rowFiltro({
        title:'Calle',
        tipo:'text'
    });
    var noExtRow = controllers.rowFiltro({
        title:'No. Ext',
        tipo:'numeric'
    });
    var referenciaRow = controllers.rowFiltro({
        title:'Referencia',
        tipo:'numeric'
    });
    var noIntRow = controllers.rowFiltro({
        title:'No. Int',
        tipo:'numeric'
    });
    var coloniaRow = controllers.rowFiltro({
        title:'Colonia',
        tipo:'text'
    });
    var cpRow = controllers.rowFiltro({
        title:'C.P.',
        tipo:'numeric'
    });
    var ciudadRow = controllers.rowFiltro({
        title:'Ciudad',
        tipo:'text'
    });
    var estadoRow = controllers.rowFiltro({
        title:'Estado',
        tipo:'text'
    });
    var paisRow = controllers.rowFiltro({
        title:'País',
        tipo:'text'
    });
    var municipioRow = controllers.rowFiltro({
        title:'Municipio/Delegación',
        tipo:'text'
    });
    var atencionRow = controllers.rowFiltro({
        title:'Atención',
        tipo:'text'
    });
    var telefonoRow = controllers.rowFiltro({
        title:'Teléfono',
        tipo:'numeric'
    });
    var faxRow = controllers.rowFiltro({
        title:'Fax',
        tipo:'numeric'
    });
    var mailRow = controllers.rowFiltro({
        title:'Mail Vtas',
        tipo:'text'
    });
    var clienteSolRow = controllers.rowFiltro({
        title:'Cliente solicita crédito',
        tipo:'switch'
    });
    var ccpaRow = controllers.rowFiltro({
        title:'Cliente realizará compras con pago anticipado',
        tipo:'switch'
    });
    
    mainView.add(nombreRow);
    mainView.add(calleRow);
    mainView.add(noExtRow);
    mainView.add(referenciaRow);
    mainView.add(noIntRow);
    mainView.add(coloniaRow);
    mainView.add(cpRow);
    mainView.add(ciudadRow);
    mainView.add(estadoRow);
    mainView.add(paisRow);
    mainView.add(municipioRow);
    mainView.add(atencionRow);
    mainView.add(telefonoRow);
    mainView.add(faxRow);
    mainView.add(mailRow);
    mainView.add(clienteSolRow);
    mainView.add(ccpaRow);
    
    
    
    var adjArButton = Ti.UI.createButton({
        title:'Adjuntar Archivos',
        width:140,
        height:37,
        bottom:5,
        right:30
    });
    adjArButton.addEventListener('click',function(e){
        
       if(adjArchObj=="") adjArchObj= require('iniciarVisita/adjArchUI');
       
       var adjWin = new adjArchObj({
           'openW':params.openW,
           'closeW':params.closeW
       });
       params.openW(adjWin);
        
    });
    
    window.add(mainView);
    window.add(adjArButton);
    
    return window;
}

module.exports = servicioUI;