
function tablaTipo(setValor,mode){
    
    var tableData;
    if(mode==1){
     tableData = [ {title: 'Todos'}, {title: 'AS-AutoServicio'}, {title: 'EXPE-Exportación Extranjera'}, {title: 'EXPN-Exportación Nacional'}, {title: 'GOB-Gobierno'}, {title: 'MAY-Mayoristas'}, {title: 'PROV-Proveedores'} ];
       
    }else if(mode==2){
       tableData = [ {title: 'Todos'}, {title: 'Si'}, {title: 'No'} ];
    }else{
      tableData = [ {title: 'Abarrotero'}, {title: 'AutoServicio'}, {title: 'Dulcero'}, {title: 'Freidor'}, {title: 'Gobierno'}, {title: 'Jugueteria'}, {title: 'Proveedores'} , {title: 'Mercería y Papelería'}, {title: 'Materias primas'}, {title: 'Semillas'}];
      
    }
    var table = Ti.UI.createTableView({
    data: tableData
    });
    table.addEventListener('click',function(e){
        //Ti.API.info("listener table "+JSON.stringify(e));
        setValor(e.row.title);
    });
    return table;
}

exports.rowFiltro = function(settings){
 
 var mainView = Ti.UI.createView({
     height:35,
     backgroundColor:'white',
     width:Ti.UI.FILL,
     borderWidth:1
 });
 var titleLbl = Ti.UI.createLabel({
     text:settings.title,
     font: {fontSize:14},
     left:3,
     width: '24%'
 });
 var content,popover,valor;
 function setValor(e){
     valor=e;
     content.setText(e);
     //Ti.API.info(valor);
 }
  
 

 switch(settings.tipo){
     
     case 'numeric':
     content = Ti.UI.createTextField({
     color: '#336699',
     right: 0,
     hintText:'123',
     textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
     keyboardType:Ti.UI.KEYBOARD_NUMBER_PAD,
     paddingRight:3,
     width: '75%'
    });
     break;
     case 'text':
     content = Ti.UI.createTextField({
     color: '#336699',
     right: 0,
     hintText:'abc',
     textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
     width: '75%',
     paddingRight:3
    });
     break;
     case 'switch':
     titleLbl.width="40%";
     content = Ti.UI.createSwitch({
         right: 0,
  value:true // mandatory property for iOS 
});

content.addEventListener('change',function(e){
  //Ti.API.info('Switch value: ' + content.value);
});
     
     break;
     case 'tipoCliente':
     content = Ti.UI.createLabel({
     text:"...",
     color: '#336699',
     textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
     right: 0,
     width: '75%',
     paddingRight:3
    });
    popover = Ti.UI.iPad.createPopover({ 
        width:300, 
        height:250,
        title:settings.title,
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    popover.add(tablaTipo(setValor,settings.mode));
     content.addEventListener('click',function(){
    popover.show({
        view:content,
        animated:true
    }); 
    popover.addEventListener('click',function(e){
        popover.hide();
    });
    });
     break;
    
     
 }

    
    mainView.add(titleLbl);
    mainView.add(content);
    mainView.getValor = function(){
        return valor;
    }
    return mainView;
};
