/*
 * Ventana Iniciar visita
 */

function iv(params){
    
    var utils = require('utilities');
    var baObj = require('iniciarVisita/buscadorClientesUI');
    var servObj = require('iniciarVisita/servicioUI');
    
    var servicioButton = Ti.UI.createButton({
    title:'Servicio',
    height:40,
    width:100
    });
    servicioButton.addEventListener('click',function(e){
        //Ti.API.info("servicio");
     var  servW = new servObj({
         openW:params.openW,
         closeW:params.closeW
     }); 
        params.openW(servW);
    });
    var window = Ti.UI.createWindow({
        title:'Iniciar Visita',
        backgroundColor:"white",
        rightNavButton: servicioButton
    });
    
//////  LEFT Side   //////

var leftView = Ti.UI.createView({
    width:(utils.width/3),
    backgroundColor:'gray',
    layout:'vertical',
    borderWidth:3,
    borderColor:'black',
    left:0,
    height:Ti.UI.FILL
});
var titleLbl = Ti.UI.createLabel({
    text:'Cliente',
    top:0,
    color:'black'
});
var search = Titanium.UI.createSearchBar({
    barColor:'#000', 
    showCancel:true,
    height:43,
    top:0,
});
var table = Ti.UI.createTableView({
    height:500,
    search:search
});
var busquedaAvButton = Ti.UI.createButton({
    title:'Búsqueda avanzada',
    height:40,
    width:100
});
busquedaAvButton.addEventListener('singletap',function(){
    
    bAW = new baObj();
    params.openW(bAW);
    
});

var ayudaButton = Ti.UI.createButton({
    title:'?',
    height:44,
    width:80,
    top:30
});
leftView.add(titleLbl);
leftView.add(table);
leftView.add(busquedaAvButton);
leftView.add(ayudaButton);


/////////////////////////////////
/////       Right Side      /////
var rightView = Ti.UI.createView({
    width:((utils.width/3)*2)+2,
    backgroundColor:'gray',
    layout:'vertical',
    borderWidth:3,
    borderColor:'black',
    right:0,
    height:Ti.UI.FILL
});
var rightTitleLbl = Ti.UI.createLabel({
    text:'Ubicación',
    top:0,
    color:'black'
});
var mapHolder = Ti.UI.createView({
    height:500,
    top:50,
    width:550
});
var mapview = Titanium.Map.createView({
    mapType: Titanium.Map.STANDARD_TYPE,
    region: {latitude:33.74511, longitude:-84.38993, 
            latitudeDelta:0.01, longitudeDelta:0.01},
    animate:true,
    regionFit:true,
    userLocation:true
});
var scanCBButton = Ti.UI.createButton({
    title:'Escanear código de barras',
    height:44,
    width:150,
    right:20,
    top:20
});

mapHolder.add(mapview);
rightView.add(rightTitleLbl);
rightView.add(mapHolder);
rightView.add(scanCBButton);

    
    window.add(leftView);
    window.add(rightView);
    return window;
}

module.exports=iv;