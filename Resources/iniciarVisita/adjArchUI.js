
function adjAr(params){
    
    var controller = require('iniciarVisita/adjArchC');
    var visorObj = require('iniciarVisita/visorImgUI');
    
    var guardarButton = Ti.UI.createButton({
       title:'Guardar'
   });
    
    var window = Ti.UI.createWindow({
        title:'Adjuntar Archivo',
        backgroundColor:'white',
        rightNavButton:guardarButton
    });
    
    var mainView = Ti.UI.createView({
        width:'85%',
        height:Ti.UI.SIZE,
        layout:'vertical',
        borderWidth:3,
        borderRadius:9,
        top:35
    });
    
    
    ///////  FILAS  //////
    
    var solCreditRow = controller.rowAdj({
        title:'Solicitud de Crédito'
    });
    var regFirRow = controller.rowAdj({
        title:'Registro de Firmas'
    });
    var fotoLocalRow = controller.rowAdj({
        title:'Fotografía Local'
    });
    var ultRecRow = controller.rowAdj({
        title:'Último recibo de teléfono o CFE'
    });
    var actaConsRow = controller.rowAdj({
        title:'Acta constitutiva'
    });
    var rfcRow = controller.rowAdj({
        title:'RFC (R1(persona moral) R2(persona física))'
    });
    
    mainView.add(solCreditRow);
    mainView.add(regFirRow);
    mainView.add(fotoLocalRow);
    mainView.add(ultRecRow);
    mainView.add(actaConsRow);
    mainView.add(rfcRow);
    
    
    ////    Vista Previa    /////
    var vistaPrevButton = Ti.UI.createButton({
        title:"Vista de Imágenes Cargadas",
        height:44,
        width:150,
        bottom:30
    });
    
    vistaPrevButton.addEventListener('click',function(e){
        var visImg = new visorObj("dagas");
        params.openW(visImg);
    });
    
    window.add(mainView);
    window.add(vistaPrevButton);
    
    return window;
    
}

module.exports = adjAr;