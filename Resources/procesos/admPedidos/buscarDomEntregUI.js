
function busDomEnt(){
    
    var window = Ti.UI.createWindow({
        title:'Buscar domicilio de entrega',
        backgroundColor:'white',
        layout:'vertical'
    });
    
    var filtrosView = Ti.UI.createView({
        height:Ti.UI.SIZE,
        width:'90%',
        layout:'vertical',
        borderRadius:9,
        top:25,
        borderWidth:1,
        backgroundColor:'gray'
    });
    
    var titleLbl = Ti.UI.createLabel({
      color:'white',
      text: 'Filtrar Por',
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      top: 10,
      height: 20,
      font:{fontSize:18, fontWeight:'bold'}
    });

    var calletf = Ti.UI.createTextField({
      width: Ti.UI.FILL, height:44,
      hintText:'Calle',
      paddingLeft:4,
      backgroundColor:'white',
      borderWidth:1
    });
    var coloniatf = Ti.UI.createTextField({
      width: Ti.UI.FILL, height:44,
      hintText:'Colonia',
      paddingLeft:4,
      backgroundColor:'white',
      borderWidth:1
    });
    var ciudadtf = Ti.UI.createTextField({
      width: Ti.UI.FILL, height:44,
      hintText:'Ciudad',
      paddingLeft:4,
      backgroundColor:'white',
      borderWidth:1
    });
    var muntf = Ti.UI.createTextField({
      width: Ti.UI.FILL, height:44,
      hintText:'Municipio/Delegación',
      paddingLeft:4,
      backgroundColor:'white',
      borderWidth:1
    });
    var paistf = Ti.UI.createTextField({
      width: Ti.UI.FILL, height:44,
      hintText:'País',
      paddingLeft:4,
      backgroundColor:'white',
      borderWidth:1
    });
    
    var aceptarButton = Ti.UI.createButton({
        width:100,
        height:44,
        title:'Aceptar',
        right:15
    });
    
    filtrosView.add(titleLbl);
    filtrosView.add(calletf);
    filtrosView.add(coloniatf);
    filtrosView.add(ciudadtf);
    filtrosView.add(muntf);
    filtrosView.add(paistf);
    filtrosView.add(aceptarButton);
    
    var titleDomLbl = Ti.UI.createLabel({
      color:'#005890',
      text: 'Domicilios',
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      top: 30,
      left:30,
      height: 23,
      font:{fontSize:22, fontWeight:'bold'}
    });
    
    
    window.add(filtrosView);
    window.add(titleDomLbl);
    return window;
    
}


module.exports = busDomEnt;