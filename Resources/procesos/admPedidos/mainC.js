
exports.statusRow = function(e){
    
 var statusTitle = Ti.UI.createLabel({
        width: '100%', height: 44,
        text:' Estatus                        ...',
        backgroundColor:'#cccccc',
        paddingLeft:3,
        left:0
    });   
    
    var tableData = [ {title: 'Por confirmar'}, {title: 'Confirmado'}, {title: 'Autorizado'}, {title: 'Con orden de entrega'}, {title: 'Con orden salida'}, {title: 'Facturado'}, {title: 'Cancelado'} ];

    var table = Ti.UI.createTableView({
    data: tableData
    });
    table.addEventListener('click',function(e){
        //Ti.API.info("listener table "+JSON.stringify(e.row.title));
        statusTitle.setText(e.row.title);
    });
    
  var  popover = Ti.UI.iPad.createPopover({ 
        width:300, 
        height:250,
        title:"Estatus",
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    popover.add(table);
     statusTitle.addEventListener('click',function(){
    popover.show({
        view:statusTitle,
        animated:true
    }); 
    popover.addEventListener('click',function(e){
        popover.hide();
    });
    });
    
    return statusTitle;
    
};


////Picker  Date    ////

exports.datePicker = function(origin){
  
  var picker = Ti.UI.createPicker({
  type:Ti.UI.PICKER_TYPE_DATE,
  minDate:new Date(1984,0,1),
  maxDate:new Date(2034,11,31),
  value:new Date(2013,4,12),
  top:0
});
picker.addEventListener('change',function(e){
    var str=JSON.stringify(e.value);
    var arr = str.substr(1,10);
  //Ti.API.info("User selected date: " +arr);
  //Ti.API.info("User selected date: " +JSON.stringify(e.value));
  
  origin.setText("  "+arr);
});

var  popover = Ti.UI.iPad.createPopover({ 
        width:300, 
        height:250,
        title:"Estatus",
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    popover.addEventListener('click',function(e){
        popover.hide();
    });
    popover.add(picker);
     origin.addEventListener('click',function(){
    popover.show({
        view:origin,
        animated:true
    }); 
    
    });


    
};

