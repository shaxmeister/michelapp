
function buscArtUI(){
    
   var window = Ti.UI.createWindow({
       backgroundColor:'white',
       title:'Buscador de Artículos'
   }); 
 
/////////////////////////////////////
////  Boton ver imagenes    /////////

var holderBotonVer = Ti.UI.createView({
    layout:'vertical',
    height:Ti.UI.SIZE,
    width:Ti.UI.SIZE,
    top:15,
    left:25
}); 

var botonVerTitle = Ti.UI.createLabel({
    text:'Ver Imágenes de Artículos'
});
var botonVerImg = Ti.UI.createView({
    height:40,
    width:80,
    backgroundColor:'green'
});
holderBotonVer.add(botonVerTitle);
holderBotonVer.add(botonVerImg);


/////////////////////////////////////
////  Filtros busqueda    /////////


var filtrosHolder = Ti.UI.createView({
    width:'60%',
    height:Ti.UI.SIZE,
    backgroundColor:'gray',
    borderRadius:9,
    top:90,
    left:25,
    layout:'vertical'
});

var filtrosLbl = Ti.UI.createLabel({
    text:'Filtros',
    color:'white',
    top:5
});

var codArtTf = Ti.UI.createTextField({
    backgroundColor:'white',
    hintText: ' Código de Artículo',
    borderWidth:1,
    height:37,
    width:'90%',
    top:7
});
var descTf = Ti.UI.createTextField({
    backgroundColor:'white',
    hintText: ' Descripción',
    height:37,
    borderWidth:1,
    width:'90%'
});
var linTf = Ti.UI.createTextField({
    backgroundColor:'white',
    hintText: ' Línea',
    height:37,
    borderWidth:1,
    width:'90%'
});
var grupoTf = Ti.UI.createTextField({
    backgroundColor:'white',
    hintText: ' Grupo',
    borderWidth:1,
    height:37,
    width:'90%'
});
var rangosTitleTf = Ti.UI.createLabel({
    backgroundColor:'#cccccc',
    text: ' Rango de Precios',
    height:37,
    borderWidth:1,
    width:'90%'
});
var deTf = Ti.UI.createTextField({
    backgroundColor:'white',
    hintText: ' De',
    borderWidth:1,
    height:37,
    width:'90%'
});
var aTf = Ti.UI.createTextField({
    backgroundColor:'white',
    hintText: ' A',
    borderWidth:1,
    height:37,
    width:'90%'
});


////////////////  PopOver   /////////////////////
      
    var tipoMonTf = Ti.UI.createLabel({
    backgroundColor:'white',
    text: ' Tipo de Moneda',
    height:40,
    borderWidth:1,
    width:'90%'
});
    var table = Ti.UI.createTableView({
    data: [ {title: 'Todas'}, {title: 'MXP'}, {title: 'USD'}]
    });
    table.addEventListener('click',function(e){
        Ti.API.info(JSON.stringify(e));
        tipoMonTf.setText(' Tipo de Moneda            '+e.row.title);
    });
    
var popover = Ti.UI.iPad.createPopover({
        width:300, 
        height:130,
        title:'Tipo de Moneda',
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    popover.addEventListener('click',function(e){
        popover.hide();
    });
    popover.add(table);
     tipoMonTf.addEventListener('click',function(){
    popover.show({
        view:tipoMonTf,
        animated:true
    }); 
    
    });


///////////////////////////////////////////
////            Imagen Articulo     //////
var artImg = Ti.UI.createImageView({
    width:300,
    height:300,
    top:90,
    right:20,
    backgroundColor:'red'
});

///////////////////////////////////////////
////            Resultados     //////

var titleresLbl = Ti.UI.createLabel({
    text:"Artículos",
    font:{fontSize:20},
    color:'#005890',
    top:432,
    left:25
});



filtrosHolder.add(filtrosLbl);
filtrosHolder.add(codArtTf);
filtrosHolder.add(descTf);
filtrosHolder.add(linTf);
filtrosHolder.add(grupoTf);
filtrosHolder.add(rangosTitleTf);
filtrosHolder.add(deTf);
filtrosHolder.add(aTf);
filtrosHolder.add(tipoMonTf);



window.add(holderBotonVer);
window.add(filtrosHolder);
window.add(artImg);
window.add(titleresLbl);

    return window;
}

module.exports = buscArtUI;