

function main(params){
    
    var controller = require('procesos/admPedidos/mainC');
    var genPedObj = "";
    
    var window = Ti.UI.createWindow({
        title : 'Administración de pedidos',
        backgroundColor:'white'
    });
    var rowsHeight=40;
    
/////           LEFT SIDE       //////

    var leftMainView = Ti.UI.createView({
        width:'33%',
        height:Ti.UI.FILL,
        left:0,
        borderWidth:2,
        borderColor:'gray',
        layout: 'vertical'
    });
    
    var leftTitle = Ti.UI.createLabel({
        width: '100%', height: rowsHeight,
        text:' Filtrar Pedidos Por',
        paddingLeft:5,
        backgroundColor:'#cccccc',
        left:0
    });
    
    var codPedidoTf = Ti.UI.createTextField({
      width: Ti.UI.FILL, height: rowsHeight,
      paddingLeft:3,
      hintText:'Código de Pedido'
    });
    var codClientTf = Ti.UI.createTextField({
      width: Ti.UI.FILL, height: rowsHeight,
      paddingLeft:3,
      backgroundColor:'#cccccc',
      hintText:'Código de Cliente'
    });
    var nomClientTf = Ti.UI.createTextField({
      width: Ti.UI.FILL, height: rowsHeight,
      paddingLeft:3,
      hintText:'Nombre de Cliente'
    });
    
    var fechaPedidView = Ti.UI.createView({
        width: Ti.UI.FILL, height: rowsHeight,
        backgroundColor:'gray'
    });
    var fechaPedidTitle = Ti.UI.createLabel({
        width: '50%', height: rowsHeight,
        text:' Fecha de Pedido',
        paddingLeft:3,
        left:0
    });
    var basicSwitch = Ti.UI.createSwitch({
    value:false,
    right:0
    });
    fechaPedidView.add(fechaPedidTitle);
    fechaPedidView.add(basicSwitch);
    
    var fpDeTf = Ti.UI.createLabel({
      width: Ti.UI.FILL, height: rowsHeight,
      text:' De'
    });
    controller.datePicker(fpDeTf);
    var fpATf = Ti.UI.createLabel({
      width: Ti.UI.FILL, height: rowsHeight,
      text:' A',
      backgroundColor:'#cccccc'
    });
    controller.datePicker(fpATf);
    var fechaEntregView = Ti.UI.createView({
        width: Ti.UI.FILL, height: rowsHeight,
        backgroundColor:'gray'
    });
    var fechaEntregTitle = Ti.UI.createLabel({
        width: '50%', height: rowsHeight,
        text:' Fecha de Entrega',
        paddingLeft:3,
        left:0
    });
    var basicSwitch2 = Ti.UI.createSwitch({
    value:false,
    right:0
    });
    fechaEntregView.add(fechaEntregTitle);
    fechaEntregView.add(basicSwitch2);
    
    var feDeTf = Ti.UI.createLabel({
      width: Ti.UI.FILL, height: rowsHeight,
      backgroundColor:'#cccccc',
      text:' De'
    });
    controller.datePicker(feDeTf);
    var feATf = Ti.UI.createLabel({
      width: Ti.UI.FILL, height: rowsHeight,
      text:' A'
    });
    controller.datePicker(feATf);

    var statusTitle = controller.statusRow({
        retorno : function(e){}
    });
    
    var buscarbutton = Ti.UI.createButton({
        title:'Buscar',
        height:44,
        width:100,
        top:15,
        right:15
    });
    
    
    leftMainView.add(leftTitle);
    leftMainView.add(codPedidoTf);
    leftMainView.add(codClientTf);
    leftMainView.add(nomClientTf);
    leftMainView.add(fechaPedidView);
    leftMainView.add(fpDeTf);
    leftMainView.add(fpATf);
    leftMainView.add(fechaEntregView);
    leftMainView.add(feDeTf);
    leftMainView.add(feATf);
    leftMainView.add(statusTitle);
    leftMainView.add(buscarbutton);
    window.add(leftMainView);

/////           RIGHT SIDE       //////
    var rightMainView = Ti.UI.createView({
        width:'67%',
        height:Ti.UI.FILL,
        right:0,
        layout:'vertical'
    });
    
    var rightMainHeaderView = Ti.UI.createView({
        width:'100%',
        height:70,
        layout:'horizontal',
        backgroundColor:'#cccccc'
    });
    var genPedView = Ti.UI.createView({
        width:100,
        height:60,
        left:10,
        top:5,
        backgroundColor:'red'
    });
    genPedView.addEventListener('click',function(e){
        if(genPedObj=="")genPedObj=require('procesos/admPedidos/generarPedidoUI');
        var genPedido = new genPedObj({
            'openW':params.openW,
            'closeW':params.closeW
        });
        params.openW(genPedido);
    });
    
    var modPedView = Ti.UI.createView({
        width:100,
        height:60,
        left:10,
        top:5,
        backgroundColor:'green'
    });
    var verPedView = Ti.UI.createView({
        width:100,
        height:60,
        left:10,
        top:5,
        backgroundColor:'green'
    });
    rightMainHeaderView.add(genPedView);
    rightMainHeaderView.add(modPedView);
    rightMainHeaderView.add(verPedView);
    rightMainView.add(rightMainHeaderView);
    window.add(rightMainView);
    
    return window;
}

module.exports = main;
