
function generarPedido(params){
    
    var utilities = require('utilities');
    var buscDomEntreObj = require('procesos/admPedidos/buscarDomEntregUI');
    var capturarArtObj = require('procesos/admPedidos/captArtUI');
    
    var genArtButton = Ti.UI.createButton({
        title:'Capturar Artículos'
    });
    genArtButton.addEventListener('click',function(e){
        var capArt = new capturarArtObj({
            openW:params.openW,
            closeW:params.closeW
        });
        params.openW(capArt);
    });
    
    var window = Ti.UI.createWindow({
        title:"Generar Pedido",
        backgroundColor:'white',
        layout:'vertical',
        rightNavButton : genArtButton
    });
    
    var scrollView = Ti.UI.createScrollView({
      contentWidth: utilities.width,
      contentHeight: 'auto',
      showVerticalScrollIndicator: true,
      showHorizontalScrollIndicator: false,
      height: '100%',
      width: utilities.width,
      layout:'vertical'
    });
    
/////  Primer bloque    ///////

var bloque1 = Ti.UI.createView({
    width:'90%',
    height:Ti.UI.SIZE,
    top:20,
    borderWidth:2,
    borderRadius:9,
    layout:'vertical'
});

var folPedtf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Folio de Pedido',
  borderWidth:1,
  paddingLeft:4
});
var nomClientetf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Nombre de Cliente',
  paddingLeft:4,
  borderWidth:1
});

bloque1.add(folPedtf);
bloque1.add(nomClientetf);

/////  segundo bloque    ///////
var block2Lbl = Ti.UI.createLabel({
  text: 'Domicilio Fiscal',
  textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
  top: 20,
  left:20,
  font: {fontSize:18, fontWeight:'bold'},
  height: 20
});

var bloque2 = Ti.UI.createView({
    width:'90%',
    height:Ti.UI.SIZE,
    top:20,
    borderWidth:2,
    borderRadius:9,
    layout:'vertical'
});
var calletf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*Calle',
  paddingLeft:4,
  borderWidth:1
});
var coloniatf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*Colonia',
  paddingLeft:4,
  borderWidth:1
});
var municipiotf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*Municipio',
  paddingLeft:4,
  borderWidth:1
});
var cptf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*Código Postal',
  paddingLeft:4,
  borderWidth:1
});
var ciudadtf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*Ciudad',
  paddingLeft:4,
  borderWidth:1
});
var estadotf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*Estado',
paddingLeft:4,
  borderWidth:1
});
var paistf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*País',
  paddingLeft:4,
  borderWidth:1
});
var rfctf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*RFC',
  paddingLeft:4,
  borderWidth:1
});
var telefenotf = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Teléfono',
  paddingLeft:4,
  borderWidth:1
});


bloque2.add(calletf);
bloque2.add(coloniatf);
bloque2.add(municipiotf);
bloque2.add(cptf);
bloque2.add(ciudadtf);
bloque2.add(estadotf);
bloque2.add(paistf);
bloque2.add(rfctf);
bloque2.add(telefenotf);




/////  tercer bloque    ///////

var blocke3Header = Ti.UI.createView({
    height:20,
    top:20,
    left:20,
    layout: 'horizontal'
});

var block3Lbl = Ti.UI.createLabel({
  text: 'Domicilio de Entrega',
  textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
  font: {fontSize:18, fontWeight:'bold'},
  height: 20
});

var searchDomEntre = Ti.UI.createView({
    width:20,
    height:20,
    left:8,
    backgroundColor:'blue'
});
searchDomEntre.addEventListener('click',function(e){
    
    var buscDomEntr = new buscDomEntreObj();
    params.openW(buscDomEntr);
    
});

blocke3Header.add(block3Lbl);
blocke3Header.add(searchDomEntre);

var bloque3 = Ti.UI.createView({
    width:'90%',
    height:Ti.UI.SIZE,
    top:20,
    borderWidth:2,
    borderRadius:9,
    layout:'vertical'
});
var calletf3 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Calle',
  paddingLeft:4,
  borderWidth:1
});
var coloniatf3 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Colonia',
  paddingLeft:4,
  borderWidth:1
});
var municipiotf3 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Municipio',
  paddingLeft:4,
  borderWidth:1
});
var cptf3 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Código Postal',
  paddingLeft:4,
  borderWidth:1
});
var ciudadtf3 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  paddingLeft:4,
  hintText:'Ciudad',
  borderWidth:1
});
var estadotf3 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Estado',
  paddingLeft:4,
  borderWidth:1
});
var paistf3 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'País',
  paddingLeft:4,
  borderWidth:1
});
var rfctf3 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'RFC',
  paddingLeft:4,
  borderWidth:1
});
var telefenotf3 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Teléfono',
  paddingLeft:4,
  borderWidth:1
});


bloque3.add(calletf3);
bloque3.add(coloniatf3);
bloque3.add(municipiotf3);
bloque3.add(cptf3);
bloque3.add(ciudadtf3);
bloque3.add(estadotf3);
bloque3.add(paistf3);
bloque3.add(rfctf3);
bloque3.add(telefenotf3);


/////  cuarto bloque    ///////
    var bloque4 = Ti.UI.createView({
    width:'90%',
    height:Ti.UI.SIZE,
    top:20,
    borderRadius:9,
    borderWidth:2,
    layout:'vertical'
});
var fgtf4 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*Fecha de Generación',
  paddingLeft:4,
  borderWidth:1
});
var fetf4 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*Fecha de Entrega',
  paddingLeft:4,
  borderWidth:1
});
var odctf4 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Orden de Compra',
  paddingLeft:4,
  borderWidth:1
});
var tttf4 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'*Tipo de Transporte',
  paddingLeft:4,
  borderWidth:1
});
var ooetf4 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  hintText:'Observaciones Orden de Entrega',
  paddingLeft:4,
  borderWidth:1
});
var occtf4 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  paddingLeft:4,
  hintText:'Observación Cuenta por Cobrar',
  borderWidth:1
});
var nvtf4 = Ti.UI.createTextField({
  width: Ti.UI.FILL, height:44,
  paddingLeft:4,
  hintText:'Notas de Visita',
  borderWidth:1
});



bloque4.add(fgtf4);
bloque4.add(fetf4);
bloque4.add(odctf4);
bloque4.add(tttf4);
bloque4.add(ooetf4);
bloque4.add(occtf4);
bloque4.add(nvtf4);

scrollView.add(bloque1);
scrollView.add(block2Lbl);
scrollView.add(bloque2);
scrollView.add(blocke3Header);
scrollView.add(bloque3);
scrollView.add(bloque4);

window.add(scrollView); 
   
    return window;
}

module.exports = generarPedido;