
function capArt(params){
    
    var buscArtObj = "";
    
    var window = Ti.UI.createWindow({
        title:'Captura de Artículos',
        backgroundColor:'white'
    });
    
    var guardarbutton = Ti.UI.createButton({
        title:'Guardar'
    });
    window.setRightNavButton(guardarbutton);
    var codArtInputHolder = Ti.UI.createView({
        backgroundColor:'gray',
        width:Ti.UI.SIZE,
        height:44,
        borderWidth:1,
        borderRadius:9,
        layout:'horizontal',
        left:20,
        top:20
    });
    
    var corArtLbl = Ti.UI.createLabel({
        text:'Código de Artículo',
        color:'white',
        width:Ti.UI.SIZE
    });
    var inputCodArt = Ti.UI.createTextField({
        left:4,
        height:40,
        width:250,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
        hintText:'Código de Artículo',
       // backgroundColor:'white',
        top:2
    });
    var buttonca = Ti.UI.createButton({
        width:70,
        height:40,
        top:2,
        left:4,
        title:'ok'
    });
    codArtInputHolder.add(corArtLbl);
    codArtInputHolder.add(inputCodArt);
    codArtInputHolder.add(buttonca);
    
 //////////////////////////////////////////////////////////////
    
     var datosArtHolder = Ti.UI.createView({
        height:Ti.UI.SIZE,
        width:'50%',
        layout:'vertical',
        borderRadius:9,
        top:85,
        left:20,
        borderWidth:1,
        backgroundColor:'gray'
    });
    
    var filtrosHeader = Ti.UI.createView({
        height:30,
        width:"100%"
    });
    
    var filtrosTitleLbl = Ti.UI.createLabel({
        text:'Datos de Artículo',
        color:'white',
        left:10,
        width:Ti.UI.SIZE
    });
    var buttonca = Ti.UI.createButton({
        width:30,
        height:30,
        top:2,
        right:10,
        title:'+'
    });
    filtrosHeader.add(filtrosTitleLbl);
    filtrosHeader.add(buttonca);
    datosArtHolder.add(filtrosHeader);
    
    var descrHolder = Ti.UI.createView({
        backgroundColor:'white',
        borderRadius:9,
        height:Ti.UI.SIZE,
        width:Ti.UI.SIZE,
        top:4,
        bottom:6,
        layout:'vertical'
    });
    
    var filtrosDescRow = Ti.UI.createLabel({
        text:'Descripción',
        left:10,
        width:'90%',
        height:20,
        borderRadius:9
    });
    var filtrosPrecioRow = Ti.UI.createLabel({
        text:'Precio',
        left:10,
        width:'90%',
        height:20,
        borderRadius:9
    });
    var filtrosUMRow = Ti.UI.createLabel({
        text:'Unidad de Medida',
        left:10,
        width:'90%',
        height:20,
        borderRadius:9
    });
    var filtrosCUMRow = Ti.UI.createLabel({
        text:'Cantidad por Unidad de Medida',
        left:10,
        width:'90%',
        height:20,
        borderRadius:9
    });


    descrHolder.add(filtrosDescRow);
    descrHolder.add(filtrosPrecioRow);
    descrHolder.add(filtrosUMRow);
    descrHolder.add(filtrosCUMRow);
    datosArtHolder.add(descrHolder);
 //////////////////////////////////////////////////////////////
 
    var buscCodButton = Ti.UI.createButton({
        title:'Buscar Código de Artículo',
        color:'white',
        style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN,
        top:15,
        backgroundColor:'#005890',
        right:110
    });
 
 buscCodButton.addEventListener('click',function(){
     
     if(buscArtObj=="") buscArtObj = require('procesos/admPedidos/buscadorArticulosUI');
     var buscArt = new buscArtObj();
     params.openW(buscArt);
 });
 
 
    
    window.add(codArtInputHolder);
     window.add(datosArtHolder);
     window.add(buscCodButton);
    
    return window;
}

module.exports = capArt;
