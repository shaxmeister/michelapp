

function aci(params){
    
    var window = Ti.UI.createWindow({
        title:'Agregar Comunicación Interna',
        backgroundColor:'white',
        layout:'vertical'
    });
    
    var guardarBoton = Ti.UI.createButton({
        title:'Guardar'
    });
    window.setRightNavButton(guardarBoton);
 
 
 ///////////////////////////////////////////////////
 //////         Datos Generales         ///////////
    
    var titleDatosGen = Ti.UI.createLabel({
        text:'Datos Generales',
        font:{fontSize:20,fontWeight:'bold'}
    });
    
    var dGBlock = Ti.UI.createView({
        width:'90%',
        height:Ti.UI.SIZE,
        borderRadius:9,
        borderWidth:2,
        layout:'vertical'
    });
    
    var codigoTf = Ti.UI.createTextField({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        hintText:' Código',
        keyboardType:Ti.UI.KEYBOARD_NUMBER_PAD
    });
    var asuntoTf = Ti.UI.createTextField({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        hintText:' *Asunto'
    });
    
    var d = new Date();
    var fecha = " Fecha: "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear();
    var fechaGenLbl = Ti.UI.createLabel({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        text: fecha
    });
    
    var clienteLbl = Ti.UI.createLabel({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        text:' *Cliente'
    });
    var puestoLbl = Ti.UI.createLabel({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        text:' *Puesto'
    });
    var usuarioLbl = Ti.UI.createLabel({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        text:' *Usuario'
    });
    var emailLbl = Ti.UI.createLabel({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        text:' *E-Mail'
    });
    var textoLbl = Ti.UI.createLabel({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        backgroundColor:'#cccccc',
        text:' Texto'
    });
    var textArea = Ti.UI.createTextArea({
  borderWidth: 1,
  textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
  keyboardType: Ti.UI.KEYBOARD_ASCII,
   width:Ti.UI.FILL,
   height:60,
   hintText:' *Texto'
});
    
    
    dGBlock.add(codigoTf);
    dGBlock.add(asuntoTf);
    dGBlock.add(fechaGenLbl);
    dGBlock.add(clienteLbl);
    dGBlock.add(puestoLbl);
    dGBlock.add(usuarioLbl);
    dGBlock.add(emailLbl);
    dGBlock.add(textoLbl);
    dGBlock.add(textArea);
    
 //////////////////////////////////   
 //  datos   adjuntos       ///////
 
 var titleDatosAdj = Ti.UI.createLabel({
        text:'Datos Adjuntos',
        font:{fontSize:20,fontWeight:'bold'},
        top:15
    });
    
 var dABlock = Ti.UI.createView({
        width:'90%',
        height:Ti.UI.SIZE,
        borderRadius:9,
        borderWidth:2,
        layout:'vertical'
    });
    var arcLbl = Ti.UI.createLabel({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        text:' Archivo'
    });
    var imgLbl = Ti.UI.createLabel({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        text:' Imagen'
    });
    var vidLbl = Ti.UI.createLabel({
        width:Ti.UI.FILL,
        height:37,
        borderWidth: 1,
        text:' Video'
    });
    dABlock.add(arcLbl);
    dABlock.add(imgLbl);
    dABlock.add(vidLbl);
    
    window.add(titleDatosGen);
    window.add(dGBlock);
    window.add(titleDatosAdj);
    window.add(dABlock);
    
    return window;
}

module.exports = aci;