
function mainUI(params){
    
    var controller = require('procesos/comunInterna/mainC');
    var agregarComInObj = require('procesos/comunInterna/agregarComInUI');
    var respCIobj = require('procesos/comunInterna/responderComIntUI');
    
    var window = Ti.UI.createWindow({
        title:'Comunicación Interna',
        backgroundColor:'white',
        layout:'vertical'
    });
    
    var addMsj = Ti.UI.createButton({
        title:'+'
    });
    addMsj.addEventListener('click',function(){
        var agregarComIn = new agregarComInObj({
            openW:params.openW,
            closeW:params.closeW
        });
        
        params.openW(agregarComIn);
        
    });
    
    window.setRightNavButton(addMsj);
    
    
    
 //////////////////////////////////////////
 ////       Filtros Block       ///////////
 
 var filtrosBlock = Ti.UI.createView({
     backgroundColor:'gray',
     borderRadius:9,
     borderWidth:2,
     width:'90%',
     height:Ti.UI.SIZE,
     top:25,
     layout:'vertical'
 });
 
 var filtrosTitle = Ti.UI.createLabel({
     text:'Filtrar Por',
     top:5,
     color:'white',
     font:{fontSize:22}
 });
 
 var filtrosHolder = Ti.UI.createView({
     width:'95%',
     height: Ti.UI.SIZE,
     top:5,
     layout:'vertical',
     borderRadius:9,
     bottom:15
 });
 
 var tipoLbl = controller.tipo(' Tipo');
 
 var codigo = Ti.UI.createTextField({
     hintText:' Código',
     width:Ti.UI.FILL,
     height:37,
     backgroundColor:'white'
 });
 
  var rangoLbl = Ti.UI.createLabel({
     text:' Rango de Fechas',
     width:Ti.UI.FILL,
     height:37,
     backgroundColor:'#cccccc'
 });
 var deLbl = Ti.UI.createLabel({
     text:' De',
     width:Ti.UI.FILL,
     height:37,
     backgroundColor:'white'
 });
 controller.pickerFecha(deLbl);
 
 var aLbl = Ti.UI.createLabel({
     text:' A',
     width:Ti.UI.FILL,
     height:37,
     backgroundColor:'white'
 });
 controller.pickerFecha(aLbl);
 
 
 filtrosHolder.add(tipoLbl);
 filtrosHolder.add(codigo);
 filtrosHolder.add(rangoLbl);
 filtrosHolder.add(deLbl);
 filtrosHolder.add(aLbl);
 filtrosBlock.add(filtrosTitle);
 filtrosBlock.add(filtrosHolder);
 window.add(filtrosBlock);
 
 
  var notifTitle = Ti.UI.createLabel({
     text:'Notificaciones',
     width:Ti.UI.SIZE,
     height:37,
     top:15,
     left:25,
     font:{fontSize:20, fontWeight:'bold'},
     color:'#005890'
 });
 
 notifTitle.addEventListener('click',function(){
   
   var respCI = new respCIobj({
     openW:params.openW,
   closeW:params.closeW  
   });  
     params.openW(respCI); 
     
 });
 
 window.add(notifTitle);
    
    return window;
}

module.exports = mainUI;