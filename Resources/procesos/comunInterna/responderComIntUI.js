
function respCI(){
    
     var window = Ti.UI.createWindow({
        title:'Responder Comunicación Interna',
        backgroundColor:'white'
    });
    
    var infoBlock = Ti.UI.createView({
        height:Ti.UI.SIZE,
        width:400,
        borderRadius:9,
        borderWidth:2,
        left:15,
        top:15,
        layout:'vertical'
    });
    
    function createRow(title,value){
      
      var row = Ti.UI.createView({
          width:400,
          height:25,
          borderWidth:1
      });  
      
      var title = Ti.UI.createLabel({
        text:title,
        left:0,
        width:Ti.UI.SIZE,
        font:{fontSize:16}
    });  
        
      var value = Ti.UI.createLabel({
        text:value,
        right:0,
        width:Ti.UI.SIZE,
        font:{fontSize:16},
        color:'#005890'
    });  
    
    row.add(title); 
    row.add(value); 
    return row;
    }
    
    var filas = [' Tipo',' Asunto',' Usuario que generó',' Fecha de Generación', ' Código'];
    
    for(var i=0;i<filas.length;i++){
        
        var r = createRow(filas[i],'N/A');
        infoBlock.add(r);
    }
    
    var respButton = Ti.UI.createButton({
        height:38,
        width:120,
        title:'Responder',
        right:30,
        top:120
    });
    
    window.add(infoBlock);
    window.add(respButton);
    return window;
    
}

module.exports = respCI;