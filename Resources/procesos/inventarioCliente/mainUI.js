
function main(params){
    
    var buscArtObj = require('procesos/admPedidos/buscadorArticulosUI');
    
    var window = Ti.UI.createWindow({
        title:'Inventario de Cliente',
        backgroundColor:'white',
        layout:'vertical'
    });
    
    var guardarButton = Ti.UI.createButton({
        title:'Guardar'
    });
    window.setRightNavButton(guardarButton);
    
    var filtroBlock = Ti.UI.createView({
        height:300,
        width:'90%',
        layout:'vertical',
        backgroundColor:'gray',
        borderWidth:2,
        borderRadius:9,
        top:15
    });
    var filtrosTitle = Ti.UI.createLabel({
        text:'Agregar producto:',
        left:20,
        top:10,
        font:{fontSize:19,fontWeight:'bold'},
        color:'white'
    });
    
    var datosHolder = Ti.UI.createView({
        width:'94%',
        height:Ti.UI.SIZE,
        backgroundColor:'white',
        borderRadius:9,
        borderWidth:2,
        top:15,
        layout:'vertical'
    });
    
    var productoLbl = Ti.UI.createLabel({
        height:39,
        width:Ti.UI.FILL,
        backgroundColor:'olive',
        text:' Producto'
    });
    
    productoLbl.addEventListener('click',function(){
         var buscArt = new buscArtObj();
     params.openW(buscArt);
    });
    
     
     
    var umLbl = Ti.UI.createLabel({
        height:39,
        width:Ti.UI.FILL,
        text:' Unidad de medida'
    });
    var cantLbl = Ti.UI.createTextField({
        height:39,
        width:Ti.UI.FILL,
        hintText:' Cantidad'
    });
    datosHolder.add(productoLbl);
    datosHolder.add(umLbl);
    datosHolder.add(cantLbl);
    
    
    var agregarButton = Ti.UI.createButton({
        top:20,
        title:'Agregar'
    });
    
    filtroBlock.add(filtrosTitle);
    filtroBlock.add(datosHolder);
    filtroBlock.add(agregarButton);
    
    
    
    var prodTitle = Ti.UI.createLabel({
        text:'Productos',
        font:{fontSize:22,fontWeight:'bold'},
        color:'#005890',
        top:25
    });
    
    var patvHeader = Ti.UI.createTableViewRow({
        title:'Código       Producto         Descripción           Cantidad        Unidad de Medida',
    });
    
    var prodAgregadosTv = Ti.UI.createTableView({
        borderWidth:2,
        height:200,
        width:'90%'
    });
    prodAgregadosTv.appendRow(patvHeader);
    
    
    
    window.add(filtroBlock);
    window.add(prodTitle);
    window.add(prodAgregadosTv);
    
    return window;
}

module.exports = main;
