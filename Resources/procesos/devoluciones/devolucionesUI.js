
function devWindow(){
    
    var controller = require('procesos/devoluciones/devolucionesC');
    
    var window = Ti.UI.createWindow({
        title:'Devoluciones',
        backgroundColor:'white',
        layout:'vertical'
    });
    
 ////////////////////////////////////////////
 //////             HEADER          ////////////  
 
    var header = Ti.UI.createView({
        width:Ti.UI.FILL,
        height:46,
        layout:'horizontal',
        backgroundColor:'#cccccc'
    });
    
    var agregardevButton = Ti.UI.createButton({
        title:'Agregar',
        height:42,
        top:2
    });
    
    var verSolButton = Ti.UI.createButton({
        title:'Ver solicitud',
        left:10,
        height:42,
        top:2
    });
    header.add(agregardevButton);
    header.add(verSolButton);
    
    
 ////////////////////////////////////////////
 //////             FILTROS          ////////////     
    
    var outerBlock = Ti.UI.createView({
        width:350,
        height:Ti.UI.SIZE,
        backgroundColor:'gray',
        top:15,
        left:35,
        borderRadius:9,
        borderWidth:1,
        layout:'vertical'
    });
    
    var innerBlock = Ti.UI.createView({
        width:330,
        height:Ti.UI.SIZE,
        backgroundColor:'white',
        top:10,
        borderRadius:9,
        borderWidth:1,
        left:10,
        layout:'vertical'
    });
    
    var codDevtf = Ti.UI.createTextField({
       hintText:'Código de Devolución',
  width: Ti.UI.FILL, height: 38
    });
    var codDestf = Ti.UI.createTextField({
       hintText:'Código de Desarrollador de Mercado',
  width: Ti.UI.FILL, height: 38
    });
    var nomDestf = Ti.UI.createTextField({
       hintText:'Nombre de Desarrollador de Mercado',
  width: Ti.UI.FILL, height: 38
    });
    
    var rangTitle = Ti.UI.createLabel({
        backgroundColor:'#cccccc',
        text:'Rango de Fechas',
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        height:38,
        width:Ti.UI.FILL
    });
    
    var rangDe = Ti.UI.createLabel({
        text:'  De',
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        height:38,
        width:Ti.UI.FILL
    });
    controller.pickerFecha(rangDe);
    var rangA = Ti.UI.createLabel({
        text:'  A',
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        height:38,
        width:Ti.UI.FILL
    });
    
    controller.pickerFecha(rangA);
    
    var okButton = Ti.UI.createButton({
        height:37,
        width:90,
        top:8,
        title:'Ok',
        right:4,
        bottom:10,
    });
    
    
 //////////////////////////////////////////
 //////     Resultados      ////
 var titleRes = Ti.UI.createLabel({
     text:'Devoluciones',
     top:8,
     top:10,
     left:35,
     color:'#005890',
     font:{fontSize:22, fontWeight:'bold'}
 });
    
    
    innerBlock.add(codDevtf);
    innerBlock.add(codDestf);
    innerBlock.add(nomDestf);
    innerBlock.add(rangTitle);
    innerBlock.add(rangDe);
    innerBlock.add(rangA);
    outerBlock.add(innerBlock);
    outerBlock.add(okButton);
    
    window.add(header);
    window.add(outerBlock);
    window.add(titleRes);
    
    return window;
}

module.exports=devWindow;
