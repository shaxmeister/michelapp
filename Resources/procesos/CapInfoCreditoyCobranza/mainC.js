exports.pickerFecha = function(origin){
    
    var picker = Ti.UI.createPicker({
  type:Ti.UI.PICKER_TYPE_DATE,
  minDate:new Date(1984,0,1),
  maxDate:new Date(2034,11,31),
  value:new Date(2013,4,12),
  top:0
});
picker.addEventListener('change',function(e){
    var str=JSON.stringify(e.value);
    var arr = str.substr(1,10);
  //Ti.API.info("User selected date: " +arr);
  //Ti.API.info("User selected date: " +JSON.stringify(e.value));
  
  origin.setText(" Fecha  "+arr);
});

var  popover = Ti.UI.iPad.createPopover({ 
        width:300, 
        height:250,
        title:"Estatus",
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    popover.addEventListener('click',function(e){
        popover.hide();
    });
    popover.add(picker);
     origin.addEventListener('click',function(){
    popover.show({
        view:origin,
        animated:true
    }); 
    
    });


    
};


/*
 * settings{
 *  money,
 *  title,
 *  width
 * }
 */
exports.rowFactura = function(settings){
    
    var row = Ti.UI.createView({
    width:settings.width,
    height:37,
    borderWidth:1        
    });
    
    var title = Ti.UI.createLabel({
        width:(settings.width/4),
        text: settings.title,
        color:'#005890',
        left:2
    });
    
    var value = Ti.UI.createTextField({
        width:((settings.width/4)*3)-2,
        right:0,
        value: (settings.money ? '$' : '')
    });
    
    row.add(title);
    row.add(value);
    row.value=function(){
      var g = value.value;
      value.value='';
      return   g;
    };
    return row;
};


exports.observaciones = function(title){
    
    var statusTitle = Ti.UI.createLabel({
     text:title,
     width:Ti.UI.FILL,
     color:'#005890',
     borderWidth:1,
     height:37
    });   
    
    var tableData = [ {title: 'Descuento',hasCheck:false}, {title: 'Meta Mensual',hasCheck:false}, {title: 'Devolución',hasCheck:false}, {title: 'Mal estado',hasCheck:false}
    , {title: 'Maniobras',hasCheck:false}, {title: 'Faltante',hasCheck:false}, {title: 'Promoción',hasCheck:false}, {title: 'Temporada',hasCheck:false}, {title: 'Degustación',hasCheck:false}, {title: '*Otro'}];

    var table = Ti.UI.createTableView({
    data: tableData
    });
    
    ///////////////////////////////////////
 /////          Seleccion de Row       /////
    
    function addCustomRow(){
        
    var textField = Ti.UI.createTextField({
    top: 0, left: 0,height:37
    });
    var row = Ti.UI.createTableViewRow();
    row.action='tf';
    row.add(textField);
    return row;
    }
 
    function rowSel(e){
       // //Ti.API.info(JSON.stringify(e));
       
       if(e.row.title=='*Otro'){
           Ti.API.info('otro');
           var newRow = addCustomRow();
           table.insertRowBefore(e.index,newRow);
           
       }else if(e.row.action='tf'){
           
           
       }
       else{
            e.row.hasCheck=(!e.row.hasCheck);
        tableData[e.index].hasCheck=e.row.hasCheck;
       Ti.API.info('row '+tableData[e.index].title+' check '+tableData[e.index].hasCheck);
       }
       
       
    }
    table.addEventListener('click',rowSel);
    
  var  popover = Ti.UI.iPad.createPopover({ 
        width:300, 
        height:250,
        title:"Tipo",
        arrowDirection:Ti.UI.iPad.POPOVER_ARROW_DIRECTION_UP
    });
    popover.add(table);
     statusTitle.addEventListener('click',function(){
    popover.show({
        view:statusTitle,
        animated:true
    }); 
   
    });
    popover.addEventListener('hide',getObservaciones);
    function getObservaciones(){
        Ti.API.info(JSON.stringify(tableData));
        return tableData;
    }
    statusTitle.getObservaciones=getObservaciones;
    
    return statusTitle;
    
};

exports.agregarFac = function(params){
    
  var row = Ti.UI.createTableViewRow({
      title:('Código de Factura: '+params.cf+' | Importe: '+params.im+' | Forma de pago: '+params.fp+' | Importe Cobrado: '+params.ic),
      font:{fontSize:14}
  });  
  
  return row;
};
