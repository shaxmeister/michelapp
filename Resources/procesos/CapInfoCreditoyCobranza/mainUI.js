
function mainWindow(params){
    
    var controller= require('procesos/CapInfoCreditoyCobranza/mainC');
    
    var window = Ti.UI.createWindow({
        title:'Capturar Información Crédito y Cobranza',
        backgroundColor:'white'
    });
    
    var guardar = Ti.UI.createButton({
        title:'Guardar'
    });
    window.setRightNavButton(guardar);
 
  /////      master View         //////
 
 var masterView = Ti.UI.createView({
     width:'25%',
     height:Ti.UI.FILL,
     left:0,
     layout:'vertical',
     borderWidth:2
 });
 
 var infoButton = Ti.UI.createLabel({
     height:'50%',
     width:Ti.UI.FILL,
     textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
     backgroundColor:'teal',
     text:'Información General',
     action:'info'
 });
 
 
 
 var factButton = Ti.UI.createLabel({
     height:'50%',
     width:Ti.UI.FILL,
      backgroundColor:'silver',
     textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
     text:'Factura',
     action:'facturas'
 });
 

 
 masterView.add(infoButton);
 masterView.add(factButton);
 
  masterView.addEventListener('click',function(e){
      
      if(e.source.action=='facturas'){
          igView.setZIndex(1);
          facView.setZIndex(2);
      }else{
          igView.setZIndex(2);
          facView.setZIndex(1);
      }
      
  });
  
  
 /////      Informacion general         //////
 
 var igView = Ti.UI.createView({
     width:'75%',
      height:Ti.UI.FILL,
     right:0,
     layout:'vertical',
     backgroundColor:'#cccccc',
     zIndex:2
 }); 
 
 var titleIg = Ti.UI.createLabel({
     text:'Información General',
     top:10,
     color:'red'
 });
 
 var infoBlock = Ti.UI.createView({
     layout:'vertical',
     borderRadius:9,
     borderWidth:2,
     top:25,
     width:500,
     height:Ti.UI.SIZE,
     backgroundColor:'white'
 });
 
 var codtf = Ti.UI.createLabel({
     text:' Código',
     width:297,
     height:37,
     left:2
 });
 var codClientetf = Ti.UI.createLabel({
     text:' Código del Cliente',
     width:297,
     height:37,
     left:2
 });
 var nomCltf = Ti.UI.createLabel({
     text:' Nombre del Cliente',
     width:297,
     height:37,
     left:2
 });
 
 var d = new Date();
    var fechaHoy = " "+d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear();
 var fecha = Ti.UI.createLabel({
     text:'  Fecha: '+fechaHoy,
     width:297,
     height:37,
     left:2
 });
 
 infoBlock.add(codtf);
 infoBlock.add(codClientetf);
 infoBlock.add(nomCltf);
 infoBlock.add(fecha);
 
 igView.add(titleIg);
 igView.add(infoBlock);
 
 
 /////////////////////////////////////
 //////     Facturas        /////////
 var facView = Ti.UI.createView({
     width:'75%',
      height:Ti.UI.FILL,
     right:0,
     layout:'vertical',
     backgroundColor:'#cccccc',
     zIndex:1
 }); 
 
 var titleFa = Ti.UI.createLabel({
     text:'Facturas',
     top:10,
     color:'red',
     font:{fontSize:22}
 });
 
 var capturarBlock = Ti.UI.createView({
     layout:'vertical',
     borderRadius:9,
     borderWidth:2,
     top:25,
     width:600,
     height:Ti.UI.SIZE,
     backgroundColor:'white'
 });
 
var codFac = controller.rowFactura({
    title:'Código de Factura',
    money:false,
    width:598
});
 var importe = controller.rowFactura({
    title:'Importe',
    money:true,
    width:598
});
var formPago = controller.rowFactura({
    title:'Forma de Pago',
    money:false,
    width:598
});
var importCobr = controller.rowFactura({
    title:'Importe Cobrado',
    money:true,
    width:598
});
var fechaFact = Ti.UI.createLabel({
    width:Ti.UI.FILL,
    text:' Fecha',
    color:'#005890'
});
controller.pickerFecha(fechaFact);
var aut = controller.rowFactura({
    title:'Autorizó',
    money:false,
    width:598
});
var observaciones = controller.observaciones('Observaciones');

var agregarButton = Ti.UI.createButton({
    width:100,
    height:37,
    title:'+'
});
agregarButton.addEventListener('click',function(){
    
    var r = controller.agregarFac({
        cf:codFac.value(),
        im:importe.value(),
        fp:formPago.value(),
        ic:importCobr.value()
    });
    monitorFacturasAgregadas.appendRow(r);
});



var monitorFacturasAgregadas = Ti.UI.createTableView({
    width:'95%',
    height:250,
    borderWidth:2,
    borderRadius:7,
    top:60
});



 
capturarBlock.add(codFac);
capturarBlock.add(importe);
capturarBlock.add(formPago);
capturarBlock.add(importCobr);
capturarBlock.add(fechaFact);
capturarBlock.add(aut);
capturarBlock.add(observaciones);
capturarBlock.add(agregarButton);
 facView.add(titleFa);
 facView.add(capturarBlock);
 facView.add(monitorFacturasAgregadas);




    window.add(masterView);
    window.add(igView);
    window.add(facView);

    return window;
}

module.exports = mainWindow;