
function encUI(params){
    
    var aplEncObj = require('procesos/encuestas/aplicarEncUI');
    
    var window = Ti.UI.createWindow({
        title:'Responder Encuesta',
        backgroundColor:'white'
    });
    
    var encBlock = Ti.UI.createView({
        layout:'vertical',
        width:'92%',
        height:'90%',
        borderWidth:2,
        borderRadius:9,
        backgroundColor:'gray',
        top:35
    });
    
    var title = Ti.UI.createLabel({
        text:'Encuestas',
        color:'white',
        top:15,
        font:{fontSize:22, fontWeight:'bold'}
    });
    
    var tv = Ti.UI.createTableView({
        top:20,
        height:'80%',
        width:'90%',
        data:[{title:'balones'},{title:'dulces'}]
    });
    
    var aplButton = Ti.UI.createButton({
        top:20,
        title:'Aplicar',
        width:120,
        height:36
    });
    tv.addEventListener('click',function(e){
        
        Ti.API.info(e.row.title);
        
        var preguntas = [
        {pregunta:'Como te llamas?', respuestas:[{title:'Opcion 1'},{title:'Opcion 2'},{title:'Opcion 3'}]},
        {pregunta:'Le gustó?', respuestas:[{title:'Opcion 1'},{title:'Opcion 2'},{title:'Opcion 3'}]},
        {pregunta:'Compraria de nuevo?', respuestas:[{title:'Opcion 1'},{title:'Opcion 2'},{title:'Opcion 3'}]}
        ];
        
        var aplEnc = new aplEncObj({
            datos: preguntas
        });
        params.openW(aplEnc);
        
    });
    
    encBlock.add(title);
    encBlock.add(tv);
    encBlock.add(aplButton);
    
    window.add(encBlock);
    return window;
}

module.exports = encUI;
