
function aplicarEnc(params){
    
    var pregIndex=1;
    
    var window = Ti.UI.createWindow({
        title:'Aplicar Encuesta',
        backgroundColor:'white',
        layout:'vertical'
    });
    
    var finButton = Ti.UI.createButton({
        title:'Finalizar'
    });
    window.setRightNavButton(finButton);
    
    var pregAnt = Ti.UI.createLabel({
        color:'#005890',
        font:{fontSize:20,fontWeight:'bold'},
        width:400,
        height:42,
        top:15
    });
    var pregSig = Ti.UI.createLabel({
        color:'#005890',
        font:{fontSize:20,fontWeight:'bold'},
        width:400,
        height:42,
        top:25
    });
    
    var pregBlock = Ti.UI.createView({
        top:25,
        height:300,
        width:'85%',
        backgroundColor:'gray',
        layout:'vertical',
        borderWidth:2
    });
    
    var pregBlockTitle = Ti.UI.createLabel({
        color:'white',
        width:'90%',
        height:36,
        top:5
    });
    
    var opciones = Ti.UI.createTableView({
        width:'90%',
        height:'80%',
        top:10
    });
    pregBlock.add(pregBlockTitle);
    pregBlock.add(opciones);
    
    
    function repaint(){
        pregAnt.setText('Pregunta anterior: '+params.datos[pregIndex-1].pregunta);
        pregBlockTitle.setText('Pregunta : '+params.datos[pregIndex].pregunta);
        opciones.setData(params.datos[pregIndex].respuestas);
        pregSig.setText('Pregunta siguiente: '+params.datos[pregIndex+1].pregunta);
    }
    repaint();
    
    window.add(pregAnt);
    window.add(pregBlock);
    window.add(pregSig);
    
    return window;
    
}

module.exports = aplicarEnc;
