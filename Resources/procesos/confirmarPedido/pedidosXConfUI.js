
function pedXConf(params){
    
    var window = Ti.UI.createWindow({
        title:'Pedidos por Confirmar',
        backgroundColor:'white'
    });
    
    
    var confButton = Ti.UI.createButton({
        title:'Confirmar',
        width:100,
        height:40,
        right:65,
        bottom:45
    });
    
    window.add(confButton);
    
    return window;
}

module.exports = pedXConf;