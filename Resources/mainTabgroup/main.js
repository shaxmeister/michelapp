/*
 * Tabgroup principal
 */ 
 
function mtg(){
   
    
  //////        Principal TAB       //////
    
    var pwObj = require('principal/principalUI');
    var winPrin = Titanium.UI.createWindow({
        navBarHidden:true
    });

    var navPrin = Titanium.UI.iPhone.createNavigationGroup();
    function openWPrin(w){
        navPrin.open(w);
    }
    function closeWPrin(w){
        navPrin.open(w);
    }
    var principalW = new pwObj({
        openW:openWPrin,
        closeW:closeWPrin
    });
    navPrin.window=principalW;
    winPrin.add(navPrin);
    
    var principalTab = Titanium.UI.createTab({
    title:'Principal',
    window:winPrin,
    icon:'images/tabIcons/Principal.png'
    });
    
//////        Iniciar Visita TAB       //////
    
    var ivwObj = require('iniciarVisita/iniciarVisitaUI');
    
    var win1 = Titanium.UI.createWindow({
        navBarHidden:true
    });

    var navIV = Titanium.UI.iPhone.createNavigationGroup();
    function openWIV(w){
        navIV.open(w);
    }
    function closeWIV(w){
        navIV.open(w);
    }
    var inicVisW = new ivwObj({
        openW:openWIV,
        closeW:closeWIV
    });
    navIV.window=inicVisW;
    win1.add(navIV);
    
    var inicVisTab = Titanium.UI.createTab({
    title:'Iniciar Visita',
    window:win1,
    icon:'images/tabIcons/IniVisi.png'
    });
    
///////////////////////////////////////////////////////
////////    Generar Pedido Tab      //////////////

 var genPedObj=require('procesos/admPedidos/generarPedidoUI');
    
    var winGP = Titanium.UI.createWindow({
        navBarHidden:true
    });

    var navGP = Titanium.UI.iPhone.createNavigationGroup();
    function openWGP(w){
        navGP.open(w);
    }
    function closeWGP(w){
        navGP.open(w);
    }
    var genPedido = new genPedObj({
        openW:openWGP,
        closeW:closeWGP
    });
    navGP.window=genPedido;
    winGP.add(navGP);
    
    var genPedTab = Titanium.UI.createTab({
    title:'Generar Pedido',
    window:winGP,
    icon:'images/tabIcons/GenPed.png'
    });
    
    ///////////////////////////////////////////////////////
////////    Comunicacion Interna Tab      //////////////

 var comunIntObj=require('procesos/comunInterna/mainUI');
    
    var winCI = Titanium.UI.createWindow({
        navBarHidden:true
    });

    var navCI = Titanium.UI.iPhone.createNavigationGroup();
    function openWCI(w){
        navCI.open(w);
    }
    function closeWCI(w){
        navCI.open(w);
    }
    var comInt = new comunIntObj({
        openW:openWCI,
        closeW:closeWCI
    });
    navCI.window=comInt;
    winCI.add(navCI);
    
    var comIntTab = Titanium.UI.createTab({
    title:'Comunicación Interna',
    window:winCI,
    icon:'images/tabIcons/ComInt.png'
    });
        



    var main = Ti.UI.createTabGroup({
        tabs:[principalTab,inicVisTab,genPedTab,comIntTab]
    });
   
    return main;
}

module.exports = mtg;